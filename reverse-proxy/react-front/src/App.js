import 'semantic-ui-css/semantic.min.css';

import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Error from './component/Error';
import Login from './component/Login';
import Register from './component/Register';
import PagesTemplate from './component/PagesTemplate';
import FilmList from './component/FilmList';
import Config from './config';
import Communaute from './component/Communaute';
import Swipe from './component/Swipe';
import User from './component/User';
import { useSelector } from 'react-redux';
import { selectorConnectedUser } from './core/selector/user.selector';
import Home from './component/Home';
import PrivateRoute from './component/PrivateRoute';
import Amis from './component/Amis';

function App() {
    const userConnected = useSelector(selectorConnectedUser);

    return (
        <Router>
            <Routes>
                <Route exact path="/login" element={<Login />} />
                <Route exact path="/register" element={<Register />} />
                <Route
                    exact
                    path="/"
                    element={<PagesTemplate pageContent={<Home />} title="" />}
                />
                <Route
                    exact
                    path="/deja-vue"
                    element={
                        <PrivateRoute>
                            <PagesTemplate
                                pageContent={
                                    <FilmList
                                        displayMode={
                                            Config.FILM_LIST_MODE.DEJA_VUE
                                        }
                                    />
                                }
                                title="Vos film déjà vu"
                            />
                        </PrivateRoute>
                    }
                />
                <Route
                    exact
                    path="/a-voir"
                    element={
                        <PrivateRoute>
                            <PagesTemplate
                                pageContent={
                                    <FilmList
                                        displayMode={
                                            Config.FILM_LIST_MODE.A_VOIR
                                        }
                                    />
                                }
                                title="A voir"
                            />
                        </PrivateRoute>
                    }
                />
                <Route
                    exact
                    path="/communaute"
                    element={
                        <PrivateRoute>
                            <PagesTemplate
                                pageContent={<Communaute />}
                                title="Communauté"
                            />
                        </PrivateRoute>
                    }
                />
                <Route
                    exact
                    path="/swipe"
                    element={
                        <PrivateRoute>
                            <PagesTemplate
                                pageContent={<Swipe />}
                                title="Swipe"
                            />
                        </PrivateRoute>
                    }
                />
                <Route
                    exact
                    path="/edit-user"
                    element={
                        <PrivateRoute>
                            <PagesTemplate
                                pageContent={
                                    <User
                                        displayMode={
                                            Config.USER_DIPSPLAY_MODE.EDIT_FORM
                                        }
                                        user={userConnected}
                                    />
                                }
                                title="Modifier votre compte"
                            />
                        </PrivateRoute>
                    }
                />
                <Route
                    exact
                    path="/change-password"
                    element={
                        <PrivateRoute>
                            <PagesTemplate
                                pageContent={
                                    <User
                                        displayMode={
                                            Config.USER_DIPSPLAY_MODE
                                                .CHANGE_PASSWORD
                                        }
                                        user={userConnected}
                                    />
                                }
                                title="Changer votre mot de passe"
                            />
                        </PrivateRoute>
                    }
                />
                <Route
                    exact
                    path="/stats"
                    element={
                        <PrivateRoute>
                            <PagesTemplate
                                pageContent={
                                    <User
                                        displayMode={
                                            Config.USER_DIPSPLAY_MODE.STATS
                                        }
                                        user={userConnected}
                                    />
                                }
                                title="Mon profil"
                            />
                        </PrivateRoute>
                    }
                />
                <Route
                    exact
                    path="/freinds"
                    element={
                        <PrivateRoute>
                            <PagesTemplate
                                pageContent={<Amis />}
                                title="Amis"
                            />
                        </PrivateRoute>
                    }
                />
                <Route
                    path="*"
                    element={
                        <PagesTemplate pageContent={<Error />} title="Erreur" />
                    }
                />
            </Routes>
        </Router>
    );
}

export default App;
