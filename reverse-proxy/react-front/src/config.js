const myConstClass = {
    USER_DIPSPLAY_MODE: {
        SIDEBAR: 'sidebar',
        HEADER: 'header',
        CHAT: 'chat',
        EDIT_FORM: 'editForm',
        CHANGE_PASSWORD: 'changePassword',
        STATS: 'stats',
        FREINDS: 'freinds',
    },
    FILM_LIST_MODE: {
        DEJA_VUE: 'deja-vue',
        A_VOIR: 'a-voir',
        SWIPE: 'swipe',
        DETAIL: 'detail',
    },
    SEARCH_BAR_MODE: {
        REFRESH: 'refresh',
        SORT: 'sort',
        FREINDS: 'freinds',
    },
    API_PATHS: {
        BASE: 'http://localhost/api/',
        UTILISATEUR: 'utilisateurs/',
        AUTH: 'auth/',
        FILMS: 'films/',
        CATEGORISATIONS: 'categorisations/',
        PUBLICATIONS: 'publications/',
        RECOMMANDATIONS: 'recommandations/',
        AMIS: 'amis/',
    },
    CATEGORISATIONS: {
        NON: {
            LABEL: 'Non',
            VALUE: '2',
        },
        DEJA_VUE: {
            LABEL: 'DejaVu',
            VALUE: '1',
        },
        VOIR: {
            LABEL: 'Voir',
            VALUE: '0',
        },
    },
    SOCKET_PATH: 'http://localhost:5000',
    SOCKET_EVENT: {
        FILMS_RECOMMANDES: 'movies_predict',
        USER_LOGIN: 'user-login',
        CONFIRMATION_MESSAGE: 'confirmation-message',
        FORBIDDEN: 'forbidden',
    },
    PASSWORD_REGEX:
        /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$ %^&*-]).{8,}$/,
};

export default myConstClass;
