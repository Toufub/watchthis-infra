import Config from '../config';

const RecommandationService = {
    getRecommandation(token, dispatch) {
        fetch(Config.API_PATHS.BASE + Config.API_PATHS.RECOMMANDATIONS, {
            method: 'GET',
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
                Accept: 'application/json; charset=UTF-8',
                Authorization: 'Bearer ' + token,
            },
        })
            .then((response) => {
                if (!response.ok) {
                    return Promise.reject(response);
                }
                return response.text();
            })
            .then((reponse) => {
                console.log(reponse);
            })
            .catch((error) => {
                error.text().then((errorTxt) => {
                    alert(
                        'Une erreur est survenue lors de la récuperation des recommandations : ' +
                            errorTxt
                    );
                });
            });
    },
};

export default RecommandationService;
