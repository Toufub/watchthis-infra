import Config from '../config';
import { setPublication } from '../core/actions/publications.action';
import FilmService from './filmService';
import UserService from './userService';

const PublicationService = {
    postPublication(
        token,
        rate,
        comment,
        filmId,
        inErrorCallback,
        onSuccessCallback
    ) {
        if (rate !== 0) {
            const publication = {
                filmId: filmId,
                note: rate,
                commentaire: comment,
            };
            fetch(Config.API_PATHS.BASE + Config.API_PATHS.PUBLICATIONS, {
                method: 'POST',
                headers: {
                    'Content-type': 'application/json; charset=UTF-8',
                    Accept: 'application/json; charset=UTF-8',
                    Authorization: 'Bearer ' + token,
                },
                body: JSON.stringify(publication),
            })
                .then((response) => {
                    if (!response.ok) {
                        return Promise.reject(response);
                    }
                    return response.text();
                })
                .then((reponse) => {
                    console.log(reponse);
                    onSuccessCallback();
                })
                .catch((error) => {
                    error.text().then((errorTxt) => {
                        inErrorCallback(
                            'Une erreur est survenue lors de la publication de votre commentaire : ' +
                                errorTxt
                        );
                    });
                });
        } else {
            inErrorCallback('La note minimal est de 1');
        }
    },
    getAllPublication(token, inErrorCallback, dispatch) {
        fetch(Config.API_PATHS.BASE + Config.API_PATHS.PUBLICATIONS, {
            method: 'GET',
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
                Accept: 'application/json; charset=UTF-8',
                Authorization: 'Bearer ' + token,
            },
        })
            .then((response) => {
                if (!response.ok) {
                    return Promise.reject(response);
                }
                return response.text();
            })
            .then((reponse) => {
                const publications = JSON.parse(reponse);
                var userIds = publications.map(
                    (publication) => publication.utilisateurId
                );
                userIds = userIds.filter(
                    (ele, pos) => userIds.indexOf(ele) === pos
                );

                var filmIds = publications.map(
                    (publication) => publication.filmId
                );
                filmIds = filmIds.filter(
                    (ele, pos) => filmIds.indexOf(ele) === pos
                );

                FilmService.getFilmByIdsForCommunaute(
                    token,
                    filmIds,
                    inErrorCallback,
                    dispatch
                );

                UserService.getUsersIdsByIdsForCommunaute(
                    token,
                    userIds,
                    inErrorCallback,
                    dispatch
                );
                dispatch(setPublication(publications));
            })
            .catch((error) => {
                error.text().then((errorTxt) => {
                    inErrorCallback(
                        'Une erreur est survenue lors de la publication de votre commentaire : ' +
                            errorTxt
                    );
                });
            });
    },
    getAllPublicationByUserIds(token, userIds, inErrorCallback, dispatch) {
        const ids = {
            ids: userIds,
        };
        fetch(
            Config.API_PATHS.BASE +
                Config.API_PATHS.PUBLICATIONS +
                'byutilisateurids',
            {
                method: 'POST',
                headers: {
                    'Content-type': 'application/json; charset=UTF-8',
                    Accept: 'application/json; charset=UTF-8',
                    Authorization: 'Bearer ' + token,
                },
                body: JSON.stringify(ids),
            }
        )
            .then((response) => {
                if (!response.ok) {
                    return Promise.reject(response);
                }
                return response.text();
            })
            .then((reponse) => {
                const publications = JSON.parse(reponse);

                var filmIds = publications.map(
                    (publication) => publication.filmId
                );
                filmIds = filmIds.filter(
                    (ele, pos) => filmIds.indexOf(ele) === pos
                );

                FilmService.getFilmByIdsForCommunaute(
                    token,
                    filmIds,
                    inErrorCallback,
                    dispatch
                );

                UserService.getUsersIdsByIdsForCommunaute(
                    token,
                    userIds,
                    inErrorCallback,
                    dispatch
                );
                dispatch(setPublication(publications));
            })
            .catch((error) => {
                error.text().then((errorTxt) => {
                    inErrorCallback(
                        'Une erreur est survenue lors de la publication de votre commentaire : ' +
                            errorTxt
                    );
                });
            });
    },
};

export default PublicationService;
