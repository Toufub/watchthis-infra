import Config from '../config';
import {
    connectUser,
    disconnectUser,
    setUserForPublicationsPage,
    setUserToken,
} from '../core/actions/user.actions';

const UserService = {
    connect(email, pwd, callbackError, dispatch, inSuccessCallback) {
        if (email === '' && pwd === '') {
            callbackError('Merci de remplir tous les champs du formulaire !');
        } else {
            const credential = {
                mail: email,
                pwd: pwd,
            };

            fetch(Config.API_PATHS.BASE + Config.API_PATHS.AUTH, {
                method: 'POST',
                headers: {
                    'Content-type': 'application/json; charset=UTF-8',
                    Accept: 'application/json; charset=UTF-8',
                },
                body: JSON.stringify(credential),
            })
                .then((response) => {
                    if (!response.ok) {
                        return Promise.reject(response);
                    }
                    return response.text();
                })
                .then((token) => {
                    //Enregistre le token
                    dispatch(setUserToken(token));

                    // Recuperer l'utilisateur
                    this.getCurrentUser(token, dispatch, inSuccessCallback);
                })
                .catch((error) => {
                    error.text().then((errorTxt) => {
                        callbackError(errorTxt);
                    });
                });
        }
    },
    disconnect(token, dispatch, inSuccessCallback) {
        fetch(Config.API_PATHS.BASE + Config.API_PATHS.AUTH + 'logout', {
            method: 'GET',
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
                Accept: 'application/json; charset=UTF-8',
                Authorization: 'Bearer ' + token,
            },
        })
            .then((response) => {
                if (!response.ok) {
                    return Promise.reject(response);
                }
                dispatch(disconnectUser());
                inSuccessCallback();
            })
            .catch((error) => {
                alert(
                    'une erreur est survenue lors de la deconnexion a votre compte'
                );
            });
    },
    register(
        nom,
        prenom,
        mail,
        pwd,
        pwdConfirm,
        callbackError,
        dispatch,
        inSuccess
    ) {
        if (
            nom === '' ||
            prenom === '' ||
            mail === '' ||
            pwd === '' ||
            pwdConfirm === ''
        ) {
            callbackError('Merci de remplir tous les champs du formulaire !');
        } else if (mail.length < 6 || mail.length > 100) {
            callbackError(
                'Votre email est invalide il doit contenir entre 6 et 100 caractéres'
            );
        } else if (!pwd.match(Config.PASSWORD_REGEX)) {
            callbackError(
                'Votre mots de passe doit contenir au moins 8 caractéres, 1 maj, 1 min, 1 sym et 1 num'
            );
        } else {
            if (pwd === pwdConfirm) {
                const user = {
                    Nom: nom,
                    Prenom: prenom,
                    Mail: mail,
                    Pwd: pwd,
                };

                fetch(Config.API_PATHS.BASE + Config.API_PATHS.UTILISATEUR, {
                    method: 'POST',
                    headers: {
                        'Content-type': 'application/json; charset=UTF-8',
                        Accept: 'application/json; charset=UTF-8',
                    },
                    body: JSON.stringify(user),
                })
                    .then((response) => {
                        if (!response.ok) {
                            return Promise.reject(response);
                        }
                        return response.text();
                    })
                    .then((token) => {
                        //Enregistre le token
                        dispatch(setUserToken(token));

                        // Recuperer l'utilisateur
                        this.getCurrentUser(token, dispatch, inSuccess);
                    })
                    .catch((error) => {
                        error.text().then((errorTxt) => {
                            callbackError(errorTxt);
                        });
                    });
            } else {
                callbackError(
                    'Le mots de passe et ça comfirmation ne sont pas identiques'
                );
            }
        }
    },
    getCurrentUser(token, dispatch, inSuccessCallback) {
        fetch(Config.API_PATHS.BASE + Config.API_PATHS.AUTH + 'current', {
            method: 'GET',
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
                Accept: 'application/json; charset=UTF-8',
                Authorization: 'Bearer ' + token,
            },
        })
            .then((response) => {
                if (!response.ok) {
                    return Promise.reject(response);
                }
                return response.text();
            })
            .then((user) => {
                //Enregistre l'utilisateur
                dispatch(connectUser(JSON.parse(user)));
                const jsonUser = JSON.parse(user);
                inSuccessCallback(token, jsonUser);
            })
            .catch((error) => {
                error.text().then((errorTxt) => {
                    alert(
                        'une erreur est survenue lors de la recuperation de votre compte : ' +
                            errorTxt
                    );
                });
            });
    },
    updateUser(
        token,
        dispatch,
        userId,
        nom,
        prenom,
        email,
        imageProfil,
        notificationActive,
        betaseriesId,
        callbackError,
        inSuccess
    ) {
        if (
            userId === '' ||
            nom === '' ||
            prenom === '' ||
            email === '' ||
            notificationActive === ''
        ) {
            callbackError('Merci de remplir tous les champs du formulaire !');
        } else if (email.length < 6 || email.length > 100) {
            callbackError(
                'Votre email est invalide il doit contenir entre 6 et 100 caractéres'
            );
        } else {
            const user = {
                UtilisateurId: userId,
                Nom: nom,
                Prenom: prenom,
                Mail: email,
                ImageProfil: imageProfil,
                NotificationActive: notificationActive,
                betaseriesId: betaseriesId,
            };

            fetch(
                Config.API_PATHS.BASE + Config.API_PATHS.UTILISATEUR + userId,
                {
                    method: 'PUT',
                    headers: {
                        'Content-type': 'application/json; charset=UTF-8',
                        Accept: 'application/json; charset=UTF-8',
                        Authorization: 'Bearer ' + token,
                    },
                    body: JSON.stringify(user),
                }
            )
                .then((response) => {
                    if (!response.ok) {
                        return Promise.reject(response);
                    }
                    return response.text();
                })
                .then((user) => {
                    dispatch(connectUser(JSON.parse(user)));
                    this.getCurrentUser(token, dispatch, inSuccess);
                })
                .catch((error) => {
                    error.text().then((errorTxt) => {
                        callbackError(errorTxt);
                    });
                });
        }
    },
    deleteUser(token, userId, dispatch, inSuccessCallback) {
        fetch(Config.API_PATHS.BASE + Config.API_PATHS.UTILISATEUR + userId, {
            method: 'DELETE',
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
                Accept: 'application/json; charset=UTF-8',
                Authorization: 'Bearer ' + token,
            },
        })
            .then((response) => {
                if (!response.ok) {
                    return Promise.reject(response);
                }
                return response.text();
            })
            .then((responseTxt) => {
                dispatch(disconnectUser());
                alert(
                    'Votre profile a été supprimé vous aller etre redirigé vers la page de connexion'
                );
                inSuccessCallback();
            })
            .catch((error) => {
                error.text().then((errorTxt) => {
                    alert(
                        'Une erreur est survenue lors de la suppression de votre compte :' +
                            errorTxt
                    );
                });
            });
    },
    connectToBetaSeries(login, pwd, token, dispatch, inSuccessCallback) {
        if (login !== '' && pwd !== '') {
            const cred = {
                login: login,
                password: pwd,
            };
            fetch(
                Config.API_PATHS.BASE +
                    Config.API_PATHS.UTILISATEUR +
                    'betaseries',
                {
                    method: 'POST',
                    headers: {
                        'Content-type': 'application/json; charset=UTF-8',
                        Accept: 'application/json; charset=UTF-8',
                        Authorization: 'Bearer ' + token,
                    },
                    body: JSON.stringify(cred),
                }
            )
                .then((response) => {
                    if (!response.ok) {
                        return Promise.reject(response);
                    }
                    this.getCurrentUser(token, dispatch, inSuccessCallback);
                })
                .catch((error) => {
                    error.text().then((errorTxt) => {
                        alert(
                            'Une erreur est survenue lors de la connexion a votre compte betaseries :' +
                                errorTxt
                        );
                    });
                });
        }
    },
    syncToBetaSeries(token, inSuccessCallback) {
        fetch(
            Config.API_PATHS.BASE + Config.API_PATHS.UTILISATEUR + 'betaseries',
            {
                method: 'GET',
                headers: {
                    'Content-type': 'application/json; charset=UTF-8',
                    Accept: 'application/json; charset=UTF-8',
                    Authorization: 'Bearer ' + token,
                },
            }
        )
            .then((response) => {
                if (!response.ok) {
                    return Promise.reject(response);
                }
                inSuccessCallback('Votre compte a bien été synchronisé');
            })
            .catch((error) => {
                error.text().then((errorTxt) => {
                    alert(
                        'Une erreur est survenue lors de la connexion a votre compte betaseries :' +
                            errorTxt
                    );
                });
            });
    },
    getUsersIdsByIdsForCommunaute(token, userIds, inErrorCallback, dispatch) {
        const ids = {
            ids: userIds,
        };
        fetch(Config.API_PATHS.BASE + Config.API_PATHS.UTILISATEUR + 'byids', {
            method: 'POST',
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
                Accept: 'application/json; charset=UTF-8',
                Authorization: 'Bearer ' + token,
            },
            body: JSON.stringify(ids),
        })
            .then((response) => {
                if (!response.ok) {
                    return Promise.reject(response);
                }
                return response.text();
            })
            .then((user) => {
                const usersJSON = JSON.parse(user);
                const userTab = [];
                usersJSON.forEach((user) => {
                    userTab[user.utilisateurId] = user;
                });
                dispatch(setUserForPublicationsPage(userTab));
            })
            .catch((error) => {
                error.text().then((errorTxt) => {
                    inErrorCallback(
                        'Une erreur est survenue lors de la recuperation de vos films : ' +
                            errorTxt
                    );
                });
            });
    },
    getUserBySearch(token, search, inSuccess, setError) {
        fetch(
            Config.API_PATHS.BASE +
                Config.API_PATHS.UTILISATEUR +
                'search/' +
                search,
            {
                method: 'GET',
                headers: {
                    'Content-type': 'application/json; charset=UTF-8',
                    Accept: 'application/json; charset=UTF-8',
                    Authorization: 'Bearer ' + token,
                },
            }
        )
            .then((response) => {
                if (!response.ok) {
                    return Promise.reject(response);
                }
                return response.text();
            })
            .then((user) => {
                const jsonUser = JSON.parse(user);
                if (jsonUser && jsonUser.length !== 0) {
                    inSuccess(jsonUser);
                } else {
                    setError('Aucun utilisateur correspond a votre recherche');
                }
            })
            .catch((error) => {
                error.text().then((errorTxt) => {
                    alert(
                        'une erreur est survenue lors de la recuperation de votre compte : ' +
                            errorTxt
                    );
                });
            });
    },
    updatePassword(
        token,
        pwd,
        newPwd,
        confirmPwd,
        userId,
        callbackError,
        inSuccess
    ) {
        if (pwd !== '' && newPwd !== '' && confirmPwd !== '') {
            if (!newPwd.match(Config.PASSWORD_REGEX)) {
                callbackError(
                    'Votre mots de passe doit contenir au moins 8 caractéres, 1 maj, 1 min, 1 sym et 1 num'
                );
            } else {
                if (newPwd === confirmPwd) {
                    const cred = {
                        OldPwd: pwd,
                        NewPwd: newPwd,
                    };

                    fetch(
                        Config.API_PATHS.BASE +
                            Config.API_PATHS.UTILISATEUR +
                            'password/' +
                            userId,
                        {
                            method: 'PUT',
                            headers: {
                                'Content-type':
                                    'application/json; charset=UTF-8',
                                Accept: 'application/json; charset=UTF-8',
                                Authorization: 'Bearer ' + token,
                            },
                            body: JSON.stringify(cred),
                        }
                    )
                        .then((response) => {
                            if (!response.ok) {
                                return Promise.reject(response);
                            }
                            inSuccess();
                        })
                        .catch((error) => {
                            error.text().then((errorTxt) => {
                                callbackError(
                                    'Une erreur est survenue lors du changement de mots de passe : ' +
                                        errorTxt
                                );
                            });
                        });
                } else {
                    callbackError(
                        'Le mots de passe et ça comfirmation ne sont pas identiques'
                    );
                }
            }
        } else {
            callbackError('Tous les champs du formulaire sont obligatoire !');
        }
    },
};
export default UserService;
