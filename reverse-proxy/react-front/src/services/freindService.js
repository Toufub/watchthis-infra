import Config from '../config';
import { setUserFreinds } from '../core/actions/user.actions';

const FreindsService = {
    addNewFreinds(token, userId, onSuccessCallback, dispatch) {
        const amiId = {
            amiId: userId,
        };
        fetch(
            Config.API_PATHS.BASE +
                Config.API_PATHS.UTILISATEUR +
                Config.API_PATHS.AMIS,
            {
                method: 'POST',
                headers: {
                    'Content-type': 'application/json; charset=UTF-8',
                    Accept: 'application/json; charset=UTF-8',
                    Authorization: 'Bearer ' + token,
                },
                body: JSON.stringify(amiId),
            }
        )
            .then((response) => {
                if (!response.ok) {
                    return Promise.reject(response);
                }
                return response.text();
            })
            .then((reponse) => {
                onSuccessCallback();
                this.getFreinds(token, dispatch);
            })
            .catch((error) => {
                error.text().then((errorTxt) => {
                    alert(
                        "Une erreur est survenue lors de l'ajout de votre amis '" +
                            errorTxt
                    );
                });
            });
    },
    getFreinds(token, dispatch) {
        fetch(
            Config.API_PATHS.BASE +
                Config.API_PATHS.UTILISATEUR +
                Config.API_PATHS.AMIS +
                'getamis',
            {
                method: 'GET',
                headers: {
                    'Content-type': 'application/json; charset=UTF-8',
                    Accept: 'application/json; charset=UTF-8',
                    Authorization: 'Bearer ' + token,
                },
            }
        )
            .then((response) => {
                if (!response.ok) {
                    return Promise.reject(response);
                }
                return response.text();
            })
            .then((reponse) => {
                const json = JSON.parse(reponse);
                dispatch(setUserFreinds(json));
            })
            .catch((error) => {
                error.text().then((errorTxt) => {
                    alert(
                        "Une erreur est survenue lors de l'ajout de votre amis '" +
                            errorTxt
                    );
                });
            });
    },
    deleteFreind(token, userId, inSuccess, dispatch) {
        fetch(
            Config.API_PATHS.BASE +
                Config.API_PATHS.UTILISATEUR +
                Config.API_PATHS.AMIS +
                userId,
            {
                method: 'DELETE',
                headers: {
                    'Content-type': 'application/json; charset=UTF-8',
                    Accept: 'application/json; charset=UTF-8',
                    Authorization: 'Bearer ' + token,
                },
            }
        )
            .then((response) => {
                if (!response.ok) {
                    return Promise.reject(response);
                }
                return response.text();
            })
            .then((reponse) => {
                inSuccess();
                this.getFreinds(token, dispatch);
            })
            .catch((error) => {
                error.text().then((errorTxt) => {
                    alert(
                        "Une erreur est survenue lors de l'ajout de votre amis '" +
                            errorTxt
                    );
                });
            });
    },
};

export default FreindsService;
