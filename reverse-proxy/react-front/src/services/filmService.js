import Config from '../config';
import {
    setFilmAVoir,
    setFilmDejaVue,
    setFilmsForPublicationsPage,
} from '../core/actions/films.actions';
import { swipeSetNextFilm } from '../core/actions/swipe.actions';

const FilmService = {
    getFilmByCategorisationAndUser(
        token,
        categorie,
        inErrorCallback,
        dispatch
    ) {
        fetch(
            Config.API_PATHS.BASE +
                Config.API_PATHS.UTILISATEUR +
                Config.API_PATHS.CATEGORISATIONS +
                'ByCategorieUtilisateur/' +
                categorie,
            {
                method: 'GET',
                headers: {
                    'Content-type': 'application/json; charset=UTF-8',
                    Accept: 'application/json; charset=UTF-8',
                    Authorization: 'Bearer ' + token,
                },
            }
        )
            .then((response) => {
                if (!response.ok) {
                    return Promise.reject(response);
                }
                return response.text();
            })
            .then((films) => {
                const filmsJSON = JSON.parse(films);
                const ids = filmsJSON.map((film) => {
                    return film.filmId;
                });
                this.getFilmByIds(
                    token,
                    ids,
                    categorie,
                    inErrorCallback,
                    dispatch
                );
            })
            .catch((error) => {
                error.text().then((errorTxt) => {
                    inErrorCallback(
                        'Une erreur est survenue lors de la recuperation de vos films : ' +
                            errorTxt
                    );
                });
            });
    },
    getFilmByIds(token, filmsIds, categorie, inErrorCallback, dispatch) {
        const ids = {
            ids: filmsIds,
        };
        fetch(Config.API_PATHS.BASE + Config.API_PATHS.FILMS + 'byids', {
            method: 'POST',
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
                Accept: 'application/json; charset=UTF-8',
                Authorization: 'Bearer ' + token,
            },
            body: JSON.stringify(ids),
        })
            .then((response) => {
                if (!response.ok) {
                    return Promise.reject(response);
                }
                return response.text();
            })
            .then((films) => {
                const filmsJSON = JSON.parse(films);
                if (categorie === Config.CATEGORISATIONS.DEJA_VUE.VALUE) {
                    dispatch(setFilmDejaVue(filmsJSON));
                } else if (categorie === Config.CATEGORISATIONS.VOIR.VALUE) {
                    dispatch(setFilmAVoir(filmsJSON));
                }
            })
            .catch((error) => {
                error.text().then((errorTxt) => {
                    inErrorCallback(
                        'Une erreur est survenue lors de la recuperation de vos films : ' +
                            errorTxt
                    );
                });
            });
    },
    getFilmByIdsForCommunaute(token, filmsIds, inErrorCallback, dispatch) {
        const ids = {
            ids: filmsIds,
        };
        fetch(Config.API_PATHS.BASE + Config.API_PATHS.FILMS + 'byids', {
            method: 'POST',
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
                Accept: 'application/json; charset=UTF-8',
                Authorization: 'Bearer ' + token,
            },
            body: JSON.stringify(ids),
        })
            .then((response) => {
                if (!response.ok) {
                    return Promise.reject(response);
                }
                return response.text();
            })
            .then((films) => {
                const filmsJSON = JSON.parse(films);
                const filmsTab = [];
                filmsJSON.forEach((film) => {
                    filmsTab[film.filmId] = film;
                });
                dispatch(setFilmsForPublicationsPage(filmsTab));
            })
            .catch((error) => {
                error.text().then((errorTxt) => {
                    inErrorCallback(
                        'Une erreur est survenue lors de la recuperation de vos films : ' +
                            errorTxt
                    );
                });
            });
    },
    getFilmByIdsForSwipe(token, filmsIds, dispatch) {
        const ids = {
            ids: filmsIds,
        };
        fetch(Config.API_PATHS.BASE + Config.API_PATHS.FILMS + 'byids', {
            method: 'POST',
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
                Accept: 'application/json; charset=UTF-8',
                Authorization: 'Bearer ' + token,
            },
            body: JSON.stringify(ids),
        })
            .then((response) => {
                if (!response.ok) {
                    return Promise.reject(response);
                }
                return response.text();
            })
            .then((films) => {
                const filmsJSON = JSON.parse(films);

                dispatch(swipeSetNextFilm(filmsJSON));
            })
            .catch((error) => {
                error.text().then((errorTxt) => {
                    alert(
                        'Une erreur est survenue lors de la recuperation de vos films : ' +
                            errorTxt
                    );
                });
            });
    },
    postCategorisation(token, userId, filmId, categorie, dispatch) {
        if (
            token !== '' &&
            userId !== '' &&
            filmId !== '' &&
            categorie !== ''
        ) {
            const cat = {
                filmId: filmId,
                utilisateurId: userId,
                categorie: Number(categorie),
            };
            fetch(
                Config.API_PATHS.BASE +
                    Config.API_PATHS.UTILISATEUR +
                    Config.API_PATHS.CATEGORISATIONS,
                {
                    method: 'POST',
                    headers: {
                        'Content-type': 'application/json; charset=UTF-8',
                        Accept: 'application/json; charset=UTF-8',
                        Authorization: 'Bearer ' + token,
                    },
                    body: JSON.stringify(cat),
                }
            )
                .then((response) => {
                    if (!response.ok) {
                        return Promise.reject(response);
                    }
                    return response.text();
                })
                .then((responseTxt) => {
                    console.log(responseTxt);
                    this.refreshFilm(token, dispatch);
                })
                .catch((error) => {
                    error.text().then((errorTxt) => {
                        alert(
                            'Une erreur est survenue lors de la sauvgarde de votre avis : ' +
                                errorTxt
                        );
                    });
                });
        }
    },
    putCategorisation(token, userId, filmId, categorie, dispatch) {
        if (
            token !== '' &&
            userId !== '' &&
            filmId !== '' &&
            categorie !== ''
        ) {
            const cat = {
                filmId: filmId,
                utilisateurId: userId,
                categorie: Number(categorie),
            };
            fetch(
                Config.API_PATHS.BASE +
                    Config.API_PATHS.UTILISATEUR +
                    Config.API_PATHS.CATEGORISATIONS +
                    userId +
                    '/' +
                    filmId,
                {
                    method: 'PUT',
                    headers: {
                        'Content-type': 'application/json; charset=UTF-8',
                        Accept: 'application/json; charset=UTF-8',
                        Authorization: 'Bearer ' + token,
                    },
                    body: JSON.stringify(cat),
                }
            )
                .then((response) => {
                    if (!response.ok) {
                        return Promise.reject(response);
                    }
                    return response.text();
                })
                .then((responseTxt) => {
                    console.log(responseTxt);
                    this.refreshFilm(token, dispatch);
                })
                .catch((error) => {
                    error.text().then((errorTxt) => {
                        alert(
                            'Une erreur est survenue lors de la modification de votre avis : ' +
                                errorTxt
                        );
                    });
                });
        }
    },
    deleteAllCAtegorisation(token, userId, dispatch) {
        fetch(
            Config.API_PATHS.BASE +
                Config.API_PATHS.UTILISATEUR +
                Config.API_PATHS.CATEGORISATIONS +
                userId,
            {
                method: 'DELETE',
                headers: {
                    'Content-type': 'application/json; charset=UTF-8',
                    Accept: 'application/json; charset=UTF-8',
                    Authorization: 'Bearer ' + token,
                },
            }
        )
            .then((response) => {
                if (!response.ok) {
                    return Promise.reject(response);
                }
                return response.text();
            })
            .then((responseTxt) => {
                console.log(responseTxt);
                this.refreshFilm(token, dispatch);
            })
            .catch((error) => {
                error.text().then((errorTxt) => {
                    alert(
                        'Une erreur est survenue lors de la modification de votre avis : ' +
                            errorTxt
                    );
                });
            });
    },
    refreshFilm(token, dispatch) {
        FilmService.getFilmByCategorisationAndUser(
            token,
            Config.CATEGORISATIONS.DEJA_VUE.VALUE,
            alert,
            dispatch
        );
        FilmService.getFilmByCategorisationAndUser(
            token,
            Config.CATEGORISATIONS.VOIR.VALUE,
            alert,
            dispatch
        );
    },
};
export default FilmService;
