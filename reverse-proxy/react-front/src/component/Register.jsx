import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { Button, Header } from 'semantic-ui-react';
import UserService from '../services/userService';
import '../style/connection-form.css';

import CustomInput from './CustomInput';
function Register() {
    const [nom, setNom] = useState('');
    const [prenom, setPrenom] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPwd] = useState('');
    const [confirmPassword, setConfirmPwd] = useState('');
    const [error, setError] = useState('');

    const navigate = useNavigate();

    const dispatch = useDispatch();

    const navigateToLogin = () => {
        navigate('/login');
    };

    const inSuccessInscription = (token) => {
        navigate('/deja-vue');
    };

    const onInscriptionClick = () => {
        setError('');
        UserService.register(
            nom,
            prenom,
            email,
            password,
            confirmPassword,
            setError,
            dispatch,
            inSuccessInscription
        );
    };

    return (
        <div
            className="ui middle aligned center aligned grid"
            style={{ height: '100%' }}
        >
            <div
                className="column"
                style={{ maxWidth: '450px', marginTop: '2em' }}
            >
                <div className="connection-form">
                    <Header as="h1" className="title">
                        Inscription
                    </Header>
                    <p className="error">{error}</p>
                    <CustomInput
                        label="Nom"
                        placeholder="Doe"
                        type="text"
                        onChangeFunction={(e) => setNom(e.target.value)}
                        value={nom}
                    />
                    <CustomInput
                        label="Prenom"
                        placeholder="John"
                        type="text"
                        onChangeFunction={(e) => setPrenom(e.target.value)}
                        value={prenom}
                    />
                    <CustomInput
                        label="Email"
                        placeholder="exemple@mail.com"
                        type="text"
                        onChangeFunction={(e) => setEmail(e.target.value)}
                        value={email}
                    />
                    <CustomInput
                        label="Mot de passe"
                        placeholder="**********"
                        type="password"
                        onChangeFunction={(e) => setPwd(e.target.value)}
                        value={password}
                    />
                    <CustomInput
                        label="Confirmation mot de passe"
                        placeholder="**********"
                        type="password"
                        onChangeFunction={(e) => setConfirmPwd(e.target.value)}
                        value={confirmPassword}
                    />
                    <Button
                        circular
                        id="connection-button"
                        onClick={() => onInscriptionClick()}
                        className="btn"
                    >
                        Inscription
                    </Button>
                    <p>
                        Déjà un compte ?{' '}
                        <a onClick={() => navigateToLogin()}>Se connecter</a>
                    </p>
                </div>
            </div>
        </div>
    );
}

export default Register;
