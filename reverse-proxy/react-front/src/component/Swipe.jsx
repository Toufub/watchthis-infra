import React, { useEffect, useState } from 'react';
import FilmCard from './FilmCard';
import Config from '../config';
import { useDispatch, useSelector } from 'react-redux';
import RecommandationService from '../services/RecommandationService';
import { selectorUserToken } from '../core/selector/user.selector';
import {
    selectorNextSwipeFilm,
    selectorSwipeFilm,
    selectorSwipeIndex,
} from '../core/selector/swipe.selector';
import {
    swipeGetNextFilm,
    swipeResetIndex,
} from '../core/actions/swipe.actions';
import { Dimmer, Loader } from 'semantic-ui-react';

function Swipe() {
    const token = useSelector(selectorUserToken);
    const index = useSelector(selectorSwipeIndex);
    const swipeFilm = useSelector(selectorSwipeFilm);
    const nextSwipeFilm = useSelector(selectorNextSwipeFilm);

    const [start, setStart] = useState(true);
    const [filmInSwipe, setFilmInSwipe] = useState(null);

    const dispatch = useDispatch();

    useEffect(() => {
        if (swipeFilm.length === 0 || index === 5) {
            // recupere des films
            RecommandationService.getRecommandation(token, dispatch);
        } else if (index === swipeFilm.length) {
            //permute film
            dispatch(swipeGetNextFilm());
            //reset index
            dispatch(swipeResetIndex());
        }
        if (swipeFilm && swipeFilm[index]) {
            setFilmInSwipe(swipeFilm[index]);
        }
    }, [index, swipeFilm]);

    useEffect(() => {
        //Initialisation
        if (start && nextSwipeFilm.length !== 0) {
            setStart(false);
            dispatch(swipeGetNextFilm());
        }
    }, [nextSwipeFilm]);

    const displaySwipe = () => {
        if (filmInSwipe) {
            return (
                <FilmCard
                    displayMode={Config.FILM_LIST_MODE.SWIPE}
                    film={filmInSwipe}
                />
            );
        } else {
            return (
                <Dimmer active inverted className="dimmer-margin">
                    <Loader inverted>Loading</Loader>
                </Dimmer>
            );
        }
    };

    return displaySwipe();
}

export default Swipe;
