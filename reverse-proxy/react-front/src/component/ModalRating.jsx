import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Button, Icon, Modal, Rating } from 'semantic-ui-react';
import { closeRatingModal } from '../core/actions/modal.actions';
import {
    selectorDisplayModeInRatingModal,
    selectorFilmRatingModal,
    selectorShowRatingModal,
} from '../core/selector/modal.selector';
import CustomInput from './CustomInput';

import '../style/ratingModal.css';
import PublicationService from '../services/publicationService';
import {
    selectorConnectedUser,
    selectorUserToken,
} from '../core/selector/user.selector';
import Config from '../config';
import FilmService from '../services/filmService';
import { swipeNextIndex } from '../core/actions/swipe.actions';

function ModalRating() {
    const [commentaire, setCommentaire] = useState('');
    const [rate, setRate] = useState(0);
    const [error, setError] = useState('');

    const user = useSelector(selectorConnectedUser);
    const open = useSelector(selectorShowRatingModal);
    const filmInModal = useSelector(selectorFilmRatingModal);
    const displayMode = useSelector(selectorDisplayModeInRatingModal);

    const token = useSelector(selectorUserToken);

    const onSuccess = () => {
        setCommentaire('');
        setError('');
        dispatch(closeRatingModal());
    };

    const onPublierNote = () => {
        if (Config.FILM_LIST_MODE.SWIPE === displayMode) {
            FilmService.postCategorisation(
                token,
                user.utilisateurId,
                filmInModal.filmId,
                Config.CATEGORISATIONS.DEJA_VUE.VALUE,
                dispatch
            );
            dispatch(swipeNextIndex());
        } else if (Config.FILM_LIST_MODE.A_VOIR === displayMode) {
            FilmService.putCategorisation(
                token,
                user.utilisateurId,
                filmInModal.filmId,
                Config.CATEGORISATIONS.DEJA_VUE.VALUE,
                dispatch
            );
        }
        PublicationService.postPublication(
            token,
            rate,
            commentaire,
            filmInModal.filmId,
            setError,
            onSuccess
        );
    };

    const onClose = () => {
        setCommentaire('');
        setError('');
        dispatch(closeRatingModal());
    };

    const dispatch = useDispatch();
    return (
        <Modal
            onClose={() => onClose()}
            open={open}
            id="custom-modal"
            dimmer="blurring"
        >
            <Modal.Header>Comment notez vous ce film ?</Modal.Header>
            <Modal.Content className="" id="">
                <p className="error">{error}</p>
                <div className="w-100 rating-input">
                    <p className="mb-0 notes-label">Notes</p>
                    <Rating
                        maxRating={10}
                        defaultRating={0}
                        icon="star"
                        size="large"
                        value={rate}
                        onRate={(e, data) => {
                            setRate(data.rating);
                        }}
                    />
                </div>

                <CustomInput
                    label="Commentaire (facultatif)"
                    placeholder="Entrez votre commentaire ici..."
                    type="text"
                    onChangeFunction={(e) => setCommentaire(e.target.value)}
                    value={commentaire}
                />
            </Modal.Content>
            <Modal.Actions>
                <Button color="red" onClick={() => onClose()}>
                    <Icon name="remove" /> Retour
                </Button>
                <Button color="green" onClick={() => onPublierNote()}>
                    <Icon name="checkmark" /> Valider
                </Button>
            </Modal.Actions>
        </Modal>
    );
}

export default ModalRating;
