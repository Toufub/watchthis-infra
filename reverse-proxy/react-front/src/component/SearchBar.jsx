import React from 'react';
import { Input, Menu } from 'semantic-ui-react';
import { ArrowsUpDownIcon } from '@heroicons/react/20/solid';
import { ArrowPathIcon, PlusIcon } from '@heroicons/react/24/solid';

import Config from '../config';
import '../style/searchBar.css';

function SearchBar(props) {
    const { displayMode, onIconClick, setSearchValue } = props;

    function getActionMenuItem() {
        if (Config.SEARCH_BAR_MODE.SORT === displayMode) {
            return (
                <Menu.Item
                    position="right"
                    className="sort-container menu-item btn"
                    onClick={onIconClick}
                >
                    <ArrowsUpDownIcon className="h-6 h-6 sort-icon" />
                    <p>Notes</p>
                </Menu.Item>
            );
        } else if (Config.SEARCH_BAR_MODE.REFRESH === displayMode) {
            return (
                <Menu.Item
                    position="right"
                    className="refresh-container menu-item btn"
                    onClick={onIconClick}
                >
                    <ArrowPathIcon className="h-6 h-6 sort-icon" />
                    <p>Refresh</p>
                </Menu.Item>
            );
        } else if (Config.SEARCH_BAR_MODE.FREINDS === displayMode) {
            return (
                <Menu.Item
                    position="right"
                    className="refresh-container menu-item btn"
                    onClick={onIconClick}
                >
                    <PlusIcon className="h-6 h-6 sort-icon" />
                    <p>Ajouté</p>
                </Menu.Item>
            );
        }
    }

    return (
        <>
            <Menu className="filter-container">
                <Menu.Item className="menu-item">
                    <Input
                        className="icon"
                        icon="search"
                        placeholder="Search..."
                        onChange={(e) => setSearchValue(e.target.value)}
                    />
                </Menu.Item>
                {getActionMenuItem()}
            </Menu>
        </>
    );
}

export default SearchBar;
