import React, { useEffect, useState } from 'react';
import FilmCard from './FilmCard';
import SearchBar from './SearchBar';
import Config from '../config';
import FilmService from '../services/filmService';
import { useDispatch, useSelector } from 'react-redux';
import { selectorUserToken } from '../core/selector/user.selector';
import {
    selectorFilmAVoir,
    selectorFilmDejaVue,
} from '../core/selector/film.selector';

function FilmList(props) {
    const { displayMode } = props;

    const filmsAVoir = useSelector(selectorFilmAVoir);
    const filmsDejaVue = useSelector(selectorFilmDejaVue);

    const dispatch = useDispatch();
    const token = useSelector(selectorUserToken);

    const [films, setFilms] = useState([]);
    const [error, setError] = useState('');
    const [renderedFilms, setRenderedFilms] = useState();
    const [searchValue, setSearchValue] = useState();
    const [toggleSort, setToggleSort] = useState(true);

    useEffect(() => {
        if (displayMode === Config.FILM_LIST_MODE.DEJA_VUE) {
            setFilms(filmsDejaVue);
        } else {
            setFilms(filmsAVoir);
        }
    }, [filmsAVoir, filmsDejaVue, displayMode]);

    useEffect(() => {
        var filteredFilms = [];

        if (searchValue) {
            filteredFilms = films.filter((film) => {
                if (
                    film.titre.toLowerCase().includes(searchValue.toLowerCase())
                )
                    return film;
            });
        } else {
            filteredFilms = films;
        }

        if (toggleSort) {
            filteredFilms.sort((a, b) => (a.note > b.note ? 1 : -1));
        } else {
            filteredFilms.sort((a, b) => (a.note < b.note ? 1 : -1));
        }

        const tempRenderedFilms = filteredFilms.map((film) => {
            return (
                <FilmCard
                    displayMode={displayMode}
                    film={film}
                    key={film.filmId}
                />
            );
        });

        setRenderedFilms(tempRenderedFilms);
    }, [films, searchValue, toggleSort]);

    const functionToggleSort = () => {
        setToggleSort(!toggleSort);
    };

    return (
        <>
            <div className="ui">
                <SearchBar
                    displayMode={Config.SEARCH_BAR_MODE.SORT}
                    onIconClick={() => functionToggleSort()}
                    setSearchValue={setSearchValue}
                />
            </div>
            <div>
                <p className="error">{error}</p>
            </div>
            <div className="ui four doubling stackable cards just-content">
                {renderedFilms}
            </div>
        </>
    );
}

export default FilmList;
