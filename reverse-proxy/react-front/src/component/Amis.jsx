import { configure } from '@testing-library/react';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
    selectorConnectedUser,
    selectorUserFreinds,
    selectorUserToken,
} from '../core/selector/user.selector';
import SearchBar from './SearchBar';
import Config from '../config';
import User from './User';
import UserService from '../services/userService';
import FreindsService from '../services/freindService';
import PeopleViewer from './PeopleViewer';

import '../style/amis.css';
import ConfirmModal from './ConfirmModal';
import { Button, Card } from 'semantic-ui-react';

function Amis(props) {
    const user = useSelector(selectorConnectedUser);
    const token = useSelector(selectorUserToken);
    const userFreinds = useSelector(selectorUserFreinds);

    const dispatch = useDispatch();

    const [error, setError] = useState('');
    const [success, setSuccess] = useState('');
    const [searchValue, setSearchValue] = useState('');
    const [searchResult, setSearchResult] = useState([]);
    const [userClicked, setUserClicked] = useState();
    const [openAddUserModal, setOpenAddUserModal] = useState();
    const [renderedFreinds, setRenderedFreinds] = useState();

    const inUserFind = (users) => {
        setSearchResult(users);
    };

    const onAddUserConfirm = () => {
        FreindsService.addNewFreinds(
            token,
            userClicked.utilisateurId,
            onAddFreindsSuccess,
            dispatch
        );
    };

    const onUserClick = (user) => {
        setUserClicked(user);
        setOpenAddUserModal(true);
    };

    const onAddFreindsClick = () => {
        setError('');
        setSuccess('');
        if (searchValue) {
            UserService.getUserBySearch(token, searchValue, inUserFind, setError);
        } else {
            alert(
                'Merci de remplire le champs de rechercher pour effectuer une recherche'
            );
        }
    };

    const onAddFreindsSuccess = () => {
        setSuccess('Votre amis a bien été ajouté');
        setSearchResult(null);
    };

    const onDeleteFreind = (user) => {
        FreindsService.deleteFreind(
            token,
            user.utilisateurId,
            onDeleteFreindSuccess,
            dispatch
        );
    };

    const onDeleteFreindSuccess = () => {
        setSuccess('Votre amis a bien été supprimer');
    };

    const displaySearchResult = () => {
        if (searchResult && searchResult.length !== 0) {
            return (
                <div className="resultForm mx-1">
                    <PeopleViewer
                        title="Resultat de votre recherche : "
                        peoples={searchResult}
                        clickable
                        callback={onUserClick}
                        user
                    />
                </div>
            );
        }
    };

    useEffect(() => {
        FreindsService.getFreinds(token, dispatch);
    }, []);

    useEffect(() => {
        const temp = userFreinds.map((freind) => {
            const user = freind.sonAmi;
            return (
                <Card className="freinds">
                    <Card.Content className="center">
                        <User
                            displayMode={Config.USER_DIPSPLAY_MODE.FREINDS}
                            user={user}
                            key={user.utilisateurId}
                        />
                    </Card.Content>
                    <Card.Content className="center">
                        <Button
                            onClick={() => onDeleteFreind(user)}
                            className="btn primary-btn"
                        >
                            Supprimer
                        </Button>
                    </Card.Content>
                </Card>
            );
        });
        setRenderedFreinds(temp);
    }, [userFreinds]);

    return (
        <>
            <div className="ui">
                <div>
                    <p className="success">{success}</p>
                </div>
                <SearchBar
                    displayMode={Config.SEARCH_BAR_MODE.FREINDS}
                    onIconClick={onAddFreindsClick}
                    setSearchValue={setSearchValue}
                />
            </div>
            {displaySearchResult()}
            <div>
                <p className="error">{error}</p>
            </div>
            <div className="ui five stackable doubling cards just-content">
                {renderedFreinds}
            </div>
            <ConfirmModal
                title="Ajouter cet amis"
                content={
                    userClicked
                        ? "Vous etes sur le point d'ajouter " +
                          userClicked.nom +
                          ' comme amis '
                        : ''
                }
                callbackSuccess={onAddUserConfirm}
                setOpen={setOpenAddUserModal}
                open={openAddUserModal}
                confirmLabel="Ajouter"
            />
        </>
    );
}

export default Amis;
