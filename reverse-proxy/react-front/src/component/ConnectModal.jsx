import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { Button, Header, Modal } from 'semantic-ui-react';
import { closeConnectModal } from '../core/actions/modal.actions';
import { selectorShowConnectModal } from '../core/selector/modal.selector';
import { selectorUserToken } from '../core/selector/user.selector';
import UserService from '../services/userService';
import CustomInput from './CustomInput';

function ConnectModal() {
    const open = useSelector(selectorShowConnectModal);
    const token = useSelector(selectorUserToken);

    const [email, setEmail] = useState('');
    const [password, setPwd] = useState('');
    const [error, setError] = useState('');

    const dispatch = useDispatch();

    const navigate = useNavigate();

    const onSuccessConnexion = () => {
        dispatch(closeConnectModal());
        alert('Votre compte a bien été synchrinysée');
    };

    const onConnect = () => {
        UserService.connectToBetaSeries(
            email,
            password,
            token,
            dispatch,
            onSuccessConnexion
        );
    };

    return (
        <Modal onClose={() => dispatch(closeConnectModal())} open={open}>
            <Modal.Header>
                Connectez vous a votre compte beta-series
            </Modal.Header>
            <Modal.Content>
                <div
                    className="ui middle aligned center aligned grid"
                    style={{ height: '100%' }}
                >
                    <div
                        className="column"
                        style={{ maxWidth: '85%', marginTop: '1em' }}
                    >
                        <Header as="h1" className="title">
                            Connexion
                        </Header>
                        <p className="error">{error}</p>
                        <CustomInput
                            label="Email"
                            placeholder="exemple@mail.com"
                            type="text"
                            onChangeFunction={(e) => setEmail(e.target.value)}
                            value={email}
                        />
                        <CustomInput
                            label="Mot de passe"
                            placeholder="**********"
                            type="password"
                            onChangeFunction={(e) => setPwd(e.target.value)}
                            value={password}
                        />
                        <Button
                            circular
                            onClick={() => onConnect()}
                            className="btn primary-btn"
                        >
                            Connexion
                        </Button>
                    </div>
                </div>
            </Modal.Content>
        </Modal>
    );
}

export default ConnectModal;
