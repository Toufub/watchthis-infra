import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Message } from 'semantic-ui-react';
import {
    selectorUserToken,
    selectorUserInCommunaute,
    selectorUserFreinds,
    selectorConnectedUser,
} from '../core/selector/user.selector';
import { selectorFilmInCommunaute } from '../core/selector/film.selector';
import { selectorPublicationInCommunaute } from '../core/selector/publication.selector';
import PublicationService from '../services/publicationService';
import Chat from './Chat';
import FreindsService from '../services/freindService';

function Communaute() {
    const token = useSelector(selectorUserToken);
    const user = useSelector(selectorConnectedUser);

    const publications = useSelector(selectorPublicationInCommunaute);
    const films = useSelector(selectorFilmInCommunaute);
    const users = useSelector(selectorUserInCommunaute);

    const userFreinds = useSelector(selectorUserFreinds);

    const [error, setError] = useState('');
    const [messages, setMessages] = useState([]);

    const dispatch = useDispatch();

    useEffect(() => {
        FreindsService.getFreinds(token, dispatch);
    }, []);

    useEffect(() => {
        const ids = userFreinds.map((freind) => {
            return freind.sonAmi.utilisateurId;
        });

        ids.push(user.utilisateurId);

        PublicationService.getAllPublicationByUserIds(
            token,
            ids,
            setError,
            dispatch
        );
    }, [userFreinds]);

    useEffect(() => {
        publications.sort((a, b) => {
            return new Date(b.datePublication) - new Date(a.datePublication);
        });
        const tempMessages = publications.map((publication, index) => {
            return (
                <Chat
                    user={
                        users
                            ? users[publication.utilisateurId]
                            : publication.utilisateurId
                    }
                    film={
                        films ? films[publication.filmId] : publication.filmId
                    }
                    rate={publication.note}
                    datePublication={publication.datePublication}
                    message={publication.commentaire}
                    key={index}
                />
            );
        });
        setMessages(tempMessages);
    }, [publications, users, films]);

    return (
        <div className="ui">
            <p className="error">{error}</p>
            {messages}
        </div>
    );
}

export default Communaute;
