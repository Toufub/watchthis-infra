import React, { useEffect, useState } from 'react';
import { Image, Segment } from 'semantic-ui-react';
import HeaderSubHeader from 'semantic-ui-react/dist/commonjs/elements/Header/HeaderSubheader';

import '../style/peopleViewer.css';

function PeopleViewer(props) {
    const { title, peoples, clickable, callback, user } = props;

    const [renderedPeoples, setRenderedPeoples] = useState();

    const onPeopleClick = (people) => {
        if (clickable) {
            callback(people);
        }
    };

    const getImage = (actor) => {
        if (user && actor.imageProfil) {
            return actor.imageProfil;
        } else if (actor.personne && actor.personne.img) {
            return actor.personne.img;
        } else {
            return window.location.origin + '/images/defaultAvatar.jpg';
        }
    };

    const isUtilisateur = user ? true : false;

    useEffect(() => {
        const renderedPeoplesTemp = peoples.map((actor) => {
            const imgSrc = getImage(actor);
            return (
                <div
                    className={
                        clickable
                            ? 'people-viewer-item ui segment m-0'
                            : 'people-viewer-item'
                    }
                    key={
                        isUtilisateur
                            ? actor.utilisateurId
                            : actor.personne.personneId
                    }
                    onClick={() => onPeopleClick(actor)}
                >
                    <Image
                        src={imgSrc}
                        alt="Actor img"
                        className="people-viewer-item-img"
                        circular
                    />{' '}
                    <p>{isUtilisateur ? actor.nom : actor.personne.nom}</p>
                </div>
            );
        });
        setRenderedPeoples(renderedPeoplesTemp);
    }, [peoples]);

    return (
        <div className="pb-1">
            <HeaderSubHeader as="h5" className="mb-0">
                {title}
            </HeaderSubHeader>
            <Segment className="people-viewer-container scrolling">
                {renderedPeoples}
            </Segment>
        </div>
    );
}

export default PeopleViewer;
