import React from 'react';
import { Button, Card, Grid, Header, Image, Rating } from 'semantic-ui-react';
import { ArrowRightIcon, XCircleIcon } from '@heroicons/react/24/outline';

import Config from '../config';

import '../style/filmCard.css';
import PeopleViewer from './PeopleViewer';
import { useDispatch, useSelector } from 'react-redux';
import {
    setFilmInModal,
    setFilmInRatingModal,
} from '../core/actions/modal.actions';
import FilmService from '../services/filmService';
import {
    selectorConnectedUser,
    selectorUserToken,
} from '../core/selector/user.selector';
import { swipeNextIndex } from '../core/actions/swipe.actions';

function FilmCard(props) {
    const { displayMode, film, className } = props;

    const user = useSelector(selectorConnectedUser);
    const token = useSelector(selectorUserToken);

    const dispatch = useDispatch();

    const showDetail = () => {
        dispatch(setFilmInModal(film));
    };

    const showRatingModal = () => {
        dispatch(setFilmInRatingModal(film, displayMode));
    };

    const fakeFreinds = [
        {
            imageProfil: window.location.origin + '/images/avatarProfile.jpg',
            nom: 'Amis1',
        },
        {
            imageProfil: null,
            nom: 'Amis2',
        },
        {
            imageProfil: window.location.origin + '/images/avatarProfile.jpg',
            nom: 'Amis1',
        },
    ];

    const getImage = () => {
        if (film && film.imageUrl) {
            return film.imageUrl;
        } else {
            return window.location.origin + '/images/images-manquante.png';
        }
    };

    const getTypeLabelByValue = (val) => {
        if (val === 0) {
            return 'Film';
        } else if (val === 1) {
            return 'Court métrage';
        } else if (val === 2) {
            return 'Télé film';
        } else if (val === 3) {
            return 'Court métrage télé';
        } else {
            return 'Non definie';
        }
    };

    const onNotInterestedClick = () => {
        if (Config.FILM_LIST_MODE.SWIPE === displayMode) {
            FilmService.postCategorisation(
                token,
                user.utilisateurId,
                film.filmId,
                Config.CATEGORISATIONS.NON.VALUE,
                dispatch
            );
            dispatch(swipeNextIndex());
        } else if (Config.FILM_LIST_MODE.A_VOIR === displayMode) {
            FilmService.putCategorisation(
                token,
                user.utilisateurId,
                film.filmId,
                Config.CATEGORISATIONS.NON.VALUE,
                dispatch
            );
        }
    };

    const onInterestedClick = () => {
        if (Config.FILM_LIST_MODE.SWIPE === displayMode) {
            FilmService.postCategorisation(
                token,
                user.utilisateurId,
                film.filmId,
                Config.CATEGORISATIONS.VOIR.VALUE,
                dispatch
            );
            dispatch(swipeNextIndex());
        }
    };

    const getRealisateur = () => {
        if (film.casts && film.casts.length !== 0) {
            const reals = film.casts.filter(
                (people) => people.role === 'director'
            );
            const stringReal = reals.map((real) => {
                return real.personne.nom + ', ';
            });
            return stringReal;
        } else {
            return 'Non disponible';
        }
    };

    const getAnnee = () => {
        if (film && film.annee) {
            return film.annee;
        } else {
            return 'Non disponible';
        }
    };

    const getGenres = () => {
        return film.genre && film.genres.length !== 0
            ? film.genres.join(', ')
            : 'Non disponible';
    };

    const getDescription = () => {
        if (film && film.synopsis) {
            return film.synopsis;
        } else {
            return 'Aucune description';
        }
    };

    const getShortDescription = () => {
        if (film && film.synopsis) {
            return film.synopsis.slice(0, 100) + ' ...';
        } else {
            return 'Aucune description';
        }
    };

    const getCasting = () => {
        if (film && film.casts && film.casts.length !== 0) {
            return <PeopleViewer title="Acteur : " peoples={film.casts} />;
        } else {
            return <p className="fs-16">Aucun casting disponible</p>;
        }
    };

    function renderButtonBar() {
        if (Config.FILM_LIST_MODE.DEJA_VUE === displayMode) {
            return (
                <Button
                    className="deja-vue-btn p-1-05 btn"
                    fluid
                    onClick={() => showDetail()}
                >
                    <span className="w-100">Voir</span>

                    <ArrowRightIcon className="h-6 h-6 deja-vue-btn-icon" />
                </Button>
            );
        } else if (Config.FILM_LIST_MODE.A_VOIR === displayMode) {
            return (
                <Button.Group fluid>
                    <Button
                        className="a-voir-btn-cancel btn"
                        onClick={() => onNotInterestedClick()}
                    >
                        <XCircleIcon className="h-6 h-6 a-voir-btn-cancel-icon" />
                    </Button>
                    <Button
                        className="a-voir-btn-seen btn"
                        onClick={() => showRatingModal()}
                    >
                        Vue
                    </Button>
                    <Button
                        className="deja-vue-btn btn"
                        onClick={() => showDetail()}
                    >
                        <span className="w-100">Voir</span>
                        <ArrowRightIcon className="h-6 h-6 deja-vue-btn-icon" />
                    </Button>
                </Button.Group>
            );
        }
    }

    function renderFooterFullCard() {
        if (displayMode === Config.FILM_LIST_MODE.SWIPE) {
            return (
                <Grid divided="vertically">
                    <Grid.Row columns={3}>
                        <Grid.Column className="mb-0 px-5px">
                            <Button
                                className="a-voir-btn-cancel btn"
                                fluid
                                onClick={() => onNotInterestedClick()}
                            >
                                <XCircleIcon className="h-6 h-6 a-voir-btn-cancel-icon" />
                            </Button>
                        </Grid.Column>
                        <Grid.Column className="mb-0 px-5px">
                            <Button
                                className="a-voir-btn-seen btn h-100"
                                fluid
                                onClick={() => showRatingModal()}
                            >
                                Vue
                            </Button>
                        </Grid.Column>
                        <Grid.Column className="mb-0 px-5px">
                            <Button
                                className="deja-vue-btn btn h-100"
                                onClick={() => onInterestedClick()}
                                fluid
                            >
                                <span className="w-100">Voir</span>
                                <ArrowRightIcon className="h-6 h-6 deja-vue-btn-icon" />
                            </Button>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            );
        } else if (displayMode === Config.FILM_LIST_MODE.DETAIL) {
            return (
                <>
                    <PeopleViewer
                        title="Ces amis souhaite le voir : "
                        peoples={fakeFreinds}
                        user
                    />
                    <PeopleViewer
                        title="Ces amis l'ont déjà vu : "
                        peoples={fakeFreinds}
                        user
                    />
                </>
            );
        }
    }

    const displayInFullCard = () => {
        let addedClass =
            className !== null && displayMode === Config.FILM_LIST_MODE.DETAIL
                ? className
                : '';
        return (
            <Card fluid className={addedClass + ' max-full-card'}>
                <Image src={getImage()} wrapped ui={false} />
                <Card.Content>
                    <div className="row pb-1">
                        <Card.Header>
                            <Header as="h2">{film.titre}</Header>
                        </Card.Header>
                        <Card.Meta>
                            Type : {getTypeLabelByValue(film.type)}
                        </Card.Meta>
                        <Card.Meta>
                            {film.note}
                            <Rating
                                maxRating={1}
                                defaultRating={1}
                                icon="star"
                                size="mini"
                                disabled
                            />
                        </Card.Meta>
                    </div>
                    <div className="row pb-1">
                        <Card.Meta>Date de sortie : {getAnnee()}</Card.Meta>
                        <Card.Meta>Réalisateur : {getRealisateur()}</Card.Meta>
                        <Card.Meta>Genres : {getGenres()}</Card.Meta>
                    </div>
                    <div className="row pb-1"></div>
                    <Card.Description className="pb-1">
                        {getDescription()}
                    </Card.Description>
                    {getCasting()}
                    {renderFooterFullCard()}
                </Card.Content>
            </Card>
        );
    };

    const displayInSmallCard = () => {
        return (
            <Card>
                <Image src={getImage()} wrapped ui={false} />
                <Card.Content>
                    <Card.Header className="center">{film.titre}</Card.Header>
                </Card.Content>
                <Card.Content className="film-rating">
                    <p className="film-rating-label">Notes : </p>
                    <Rating
                        maxRating={10}
                        defaultRating={film.note}
                        icon="star"
                        size="mini"
                        disabled
                    />
                </Card.Content>
                <Card.Content>
                    <Card.Description>{getShortDescription()}</Card.Description>
                </Card.Content>
                <Card.Content extra id="btn-container">
                    {renderButtonBar()}
                </Card.Content>
            </Card>
        );
    };

    if (
        displayMode === Config.FILM_LIST_MODE.A_VOIR ||
        displayMode === Config.FILM_LIST_MODE.DEJA_VUE
    ) {
        return displayInSmallCard();
    } else if (
        displayMode === Config.FILM_LIST_MODE.SWIPE ||
        displayMode === Config.FILM_LIST_MODE.DETAIL
    ) {
        return displayInFullCard();
    }
}

export default FilmCard;
