import React from 'react';
import { useState } from 'react';
import {
    Button,
    Container,
    Dropdown,
    Header,
    Icon,
    Menu,
    Segment,
    Sidebar,
} from 'semantic-ui-react';
import { isMobile } from 'react-device-detect';
import { useDispatch, useSelector } from 'react-redux';
import {
    EyeIcon,
    FireIcon,
    PlayCircleIcon,
    UserGroupIcon,
} from '@heroicons/react/20/solid';

import '../style/pagesTemplate.css';
import Config from '../config';
import User from './User';
import CustomModal from './CustomModal';
import { useNavigate } from 'react-router-dom';
import { setActiveItem } from '../core/actions/menu.actions';
import { selectorMenuItemActif } from '../core/selector/menu.selector';
import { EllipsisVerticalIcon } from '@heroicons/react/24/solid';
import {
    selectorConnectedUser,
    selectorUserToken,
} from '../core/selector/user.selector';
import ModalRating from './ModalRating';
import UserService from '../services/userService';
import ConfirmModal from './ConfirmModal';
import { openConfirmModal } from '../core/actions/modal.actions';

function PagesTemplate(props) {
    const { pageContent, title } = props;
    const [sidebarVisible, setSidebarVisible] = useState(false);
    const activeMenuItem = useSelector(selectorMenuItemActif);
    const [openDeleteAccountModal, setOpenDeleteAccountModal] = useState(false);

    const user = useSelector(selectorConnectedUser);
    const token = useSelector(selectorUserToken);

    const dispatch = useDispatch();

    const navigate = useNavigate();

    const navigateTo = (item) => {
        dispatch(setActiveItem(item.key));
        navigate(item.link);
    };

    const navigateToLink = (link) => {
        dispatch(setActiveItem(null));
        navigate(link);
    };

    const inSuccessCallback = () => {
        navigate('/login');
    };

    const deconnexion = () => {
        UserService.disconnect(token, dispatch, inSuccessCallback);
    };

    const showConfirmModal = () => {
        //dispatch(openConfirmModal());
        setOpenDeleteAccountModal(true);
    };

    const deleteUser = () => {
        UserService.deleteUser(
            token,
            user.utilisateurId,
            dispatch,
            inSuccessCallback
        );
    };

    const displayHeader = () => {
        if (title) {
            return (
                <Header as="h3" textAlign="center" block id="page-title">
                    {title}
                </Header>
            );
        }
    };

    const menuItems = [
        {
            label: 'Déjà vu',
            icon: (
                <EyeIcon
                    className={
                        'h-6 h-6' + isMobile ? 'nav-icon-inverted' : 'nav-icon'
                    }
                />
            ),
            key: 'dejaVue',
            link: '/deja-vue',
        },
        {
            label: 'Swipe',
            icon: (
                <FireIcon
                    className={
                        'h-6 h-6' + isMobile ? 'nav-icon-inverted' : 'nav-icon'
                    }
                />
            ),
            key: 'swipe',
            link: '/swipe',
        },
        {
            label: 'A voir',
            icon: (
                <PlayCircleIcon
                    className={
                        'h-6 h-6' + isMobile ? 'nav-icon-inverted' : 'nav-icon'
                    }
                />
            ),
            key: 'aVoir',
            link: '/a-voir',
        },
        {
            label: 'Communauté',
            icon: (
                <UserGroupIcon
                    className={
                        'h-6 h-6' + isMobile ? 'nav-icon-inverted' : 'nav-icon'
                    }
                />
            ),
            key: 'communaute',
            link: '/communaute',
        },
    ];

    const renderedMenuItems = menuItems.map((item) => {
        return (
            <Menu.Item
                as="a"
                className={
                    isMobile
                        ? 'nav-item-container-mobile'
                        : 'nav-item-container'
                }
                active={activeMenuItem === item.key}
                onClick={() => navigateTo(item)}
                key={item.key}
            >
                {item.icon}
                {item.label}
            </Menu.Item>
        );
    });

    const navChildren = (
        <Container className="pb-3">
            {displayHeader()}

            {pageContent}
            <CustomModal />
            <ModalRating />
            <ConfirmModal
                title="Supprimer votre compte"
                content="Vous etes sur le point de supprimer votre compte"
                callbackSuccess={deleteUser}
                setOpen={setOpenDeleteAccountModal}
                open={openDeleteAccountModal}
                confirmLabel="Supprimer"
            />
        </Container>
    );

    function renderInFunctionOfConnectedUser() {
        if (user !== null) {
            if (isMobile) {
                // In mobile
                return (
                    <>
                        <User
                            displayMode={Config.USER_DIPSPLAY_MODE.SIDEBAR}
                            user={user}
                        />
                        <Menu inverted vertical>
                            <Menu.Item
                                as="a"
                                className="nav-item-profile"
                                onClick={() => deconnexion()}
                            >
                                Se déconnecter
                            </Menu.Item>
                            <Menu.Item
                                as="a"
                                className="nav-item-profile"
                                onClick={() => navigateToLink('/edit-user')}
                            >
                                Modifier mon profil
                            </Menu.Item>
                            <Menu.Item
                                as="a"
                                className="nav-item-profile"
                                onClick={() =>
                                    navigateToLink('/change-password')
                                }
                            >
                                Changer mon mot de passe
                            </Menu.Item>
                            <Menu.Item
                                as="a"
                                className="nav-item-profile"
                                onClick={() => navigateToLink('/stats')}
                            >
                                Mon profil
                            </Menu.Item>
                            <Menu.Item
                                as="a"
                                className="nav-item-profile"
                                onClick={() => navigateToLink('/freinds')}
                            >
                                Mes amis
                            </Menu.Item>
                            <Menu.Item
                                as="a"
                                className="nav-item-profile"
                                onClick={() => showConfirmModal()}
                            >
                                Supprimer mon profil
                            </Menu.Item>
                        </Menu>
                    </>
                );
            } else {
                // Not in mobile
                return (
                    <>
                        <Menu className="m-0">
                            <User
                                displayMode={Config.USER_DIPSPLAY_MODE.HEADER}
                                user={user}
                            />
                            <Dropdown
                                item
                                direction="left"
                                className="menu-user-container"
                                icon={
                                    <EllipsisVerticalIcon className="h-6 h-6 icon" />
                                }
                            >
                                <Dropdown.Menu>
                                    <Dropdown.Item
                                        onClick={() => deconnexion()}
                                    >
                                        Se déconnecter
                                    </Dropdown.Item>
                                    <Dropdown.Item
                                        onClick={() =>
                                            navigateToLink('/edit-user')
                                        }
                                    >
                                        Modifier mon profil
                                    </Dropdown.Item>
                                    <Dropdown.Item
                                        onClick={() =>
                                            navigateToLink('/change-password')
                                        }
                                    >
                                        Changer mon mot de passe
                                    </Dropdown.Item>
                                    <Dropdown.Item
                                        onClick={() => navigateToLink('/stats')}
                                    >
                                        Mon profil
                                    </Dropdown.Item>
                                    <Dropdown.Item
                                        onClick={() =>
                                            navigateToLink('/freinds')
                                        }
                                    >
                                        Mes amis
                                    </Dropdown.Item>
                                    <Dropdown.Item
                                        onClick={() => showConfirmModal()}
                                    >
                                        Supprimer mon profil
                                    </Dropdown.Item>
                                </Dropdown.Menu>
                            </Dropdown>
                        </Menu>
                    </>
                );
            }
        } else {
            if (isMobile) {
                return (
                    <Button
                        color="teal"
                        fluid
                        id="connect-button-mobile"
                        as="a"
                        onClick={() => navigate('/login')}
                    >
                        Se connecter
                    </Button>
                );
            } else {
                return (
                    <Button
                        color="teal"
                        id="connect-button-desktop"
                        as="a"
                        onClick={() => navigate('/login')}
                    >
                        Se connecter
                    </Button>
                );
            }
        }
    }

    const displayInLargeScreen = () => {
        return (
            <>
                <nav className="nav-container">
                    <img
                        src={
                            window.location.origin + '/images/logoWatchTh!s.png'
                        }
                        alt="logo watchth!s"
                        className="nav-logo"
                    />
                    <Menu pointing secondary className="m-0">
                        {renderedMenuItems}
                    </Menu>

                    {renderInFunctionOfConnectedUser()}
                </nav>
                {navChildren}
            </>
        );
    };

    const displayInSmallScreen = () => {
        return (
            <>
                <Sidebar.Pushable as={Segment}>
                    <Sidebar
                        as={Menu}
                        animation="overlay"
                        inverted
                        onHide={() => setSidebarVisible(false)}
                        vertical
                        visible={sidebarVisible}
                        width="thin"
                    >
                        {renderInFunctionOfConnectedUser()}
                        {renderedMenuItems}
                    </Sidebar>

                    <Sidebar.Pusher
                        dimmed={sidebarVisible}
                        style={{ minHeight: '100vh' }}
                        className="pageContent w-100"
                    >
                        <nav className="nav-container w-100">
                            <img
                                src={
                                    window.location.origin +
                                    '/images/logoWatchTh!s.png'
                                }
                                alt="logo watchth!s"
                                className="nav-logo"
                            />
                            <Menu position="right" className="m-0">
                                <Menu.Item
                                    onClick={() => {
                                        setSidebarVisible(!sidebarVisible);
                                    }}
                                >
                                    <Icon name="sidebar" />
                                </Menu.Item>
                            </Menu>
                        </nav>
                        {navChildren}
                    </Sidebar.Pusher>
                </Sidebar.Pushable>
            </>
        );
    };

    if (isMobile) {
        return displayInSmallScreen();
    } else {
        return displayInLargeScreen();
    }
}

export default PagesTemplate;
