import React from 'react';
import { Card, Image, Rating } from 'semantic-ui-react';
import Config from '../config';
import '../style/chat.css';
import User from './User';

function Chat(props) {
    const { user, message, datePublication, image, film, rate } = props;

    function setImage() {
        if (image) {
            return <Image src={image} wrapped ui={false} />;
        }
    }

    function setUser() {
        if (user) {
            return (
                <User
                    displayMode={Config.USER_DIPSPLAY_MODE.CHAT}
                    user={user}
                />
            );
        }
    }

    function getDate() {
        let date = new Date(datePublication);
        return date.toLocaleDateString();
    }

    return (
        <Card fluid>
            {setImage()}
            <Card.Content>
                {setUser()}
                <Card.Meta>{getDate()}</Card.Meta>
                <Card.Description>{message}</Card.Description>
            </Card.Content>
            <Card.Content className="chat-meta">
                <Card.Meta>
                    {film
                        ? 'A ajouté ' + film.titre + ' a sa liste des films vue'
                        : 'Non disponible'}
                </Card.Meta>
                <Card.Meta>
                    {rate}
                    <Rating
                        maxRating={1}
                        defaultRating={1}
                        icon="star"
                        size="mini"
                        disabled
                    />
                </Card.Meta>
            </Card.Content>
        </Card>
    );
}

export default Chat;
