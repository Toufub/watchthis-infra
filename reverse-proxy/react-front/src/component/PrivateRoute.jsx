import React from 'react';
import { useSelector } from 'react-redux';
import { Route } from 'react-router-dom';
import { selectorConnectedUser } from '../core/selector/user.selector';
import Home from './Home';
import PagesTemplate from './PagesTemplate';

function PrivateRoute(props) {
    const { children } = props;

    const user = useSelector(selectorConnectedUser);

    return user ? children : <PagesTemplate pageContent={<Home />} title="" />;
}

export default PrivateRoute;
