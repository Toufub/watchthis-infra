import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { Button, Header } from 'semantic-ui-react';
import Config from '../config';
import {
    selectorConnectedUser,
    selectorUserToken,
} from '../core/selector/user.selector';
import FilmService from '../services/filmService';
import UserService from '../services/userService';

import '../style/connection-form.css';
import RecommandationWSService from '../ws/recommandationWSService';

import CustomInput from './CustomInput';
function Login() {
    const [email, setEmail] = useState('');
    const [password, setPwd] = useState('');
    const [error, setError] = useState('');

    const dispatch = useDispatch();

    const navigate = useNavigate();

    const navigateToRegister = () => {
        navigate('/register');
    };

    const inSuccessConnexion = (token, user) => {
        FilmService.getFilmByCategorisationAndUser(
            token,
            Config.CATEGORISATIONS.DEJA_VUE.VALUE,
            setError,
            dispatch
        );
        FilmService.getFilmByCategorisationAndUser(
            token,
            Config.CATEGORISATIONS.VOIR.VALUE,
            setError,
            dispatch
        );
        const Recommandationservice = RecommandationWSService.getInstance(
            dispatch,
            token
        );
        Recommandationservice.sendUserLogin(user.utilisateurId);
        navigate('/deja-vue');
    };

    const onConnectionClick = () => {
        setError('');
        UserService.connect(
            email,
            password,
            setError,
            dispatch,
            inSuccessConnexion
        );
    };

    return (
        <div
            className="ui middle aligned center aligned grid"
            style={{ height: '100%' }}
        >
            <div
                className="column"
                style={{ maxWidth: '450px', marginTop: '10em' }}
            >
                <div className="connection-form">
                    <Header as="h1" className="title">
                        Connexion
                    </Header>
                    <p className="error">{error}</p>
                    <CustomInput
                        label="Email"
                        placeholder="exemple@mail.com"
                        type="text"
                        onChangeFunction={(e) => setEmail(e.target.value)}
                        value={email}
                    />
                    <CustomInput
                        label="Mot de passe"
                        placeholder="**********"
                        type="password"
                        onChangeFunction={(e) => setPwd(e.target.value)}
                        value={password}
                    />
                    <Button
                        circular
                        id="connection-button"
                        onClick={() => onConnectionClick()}
                        className="btn"
                    >
                        Connexion
                    </Button>
                    <p>
                        {' '}
                        Pas encore de compte ?{' '}
                        <a onClick={() => navigateToRegister()}>S'inscrire</a>
                    </p>
                </div>
            </div>
        </div>
    );
}

export default Login;
