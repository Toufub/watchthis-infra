import React from 'react';
import { Input } from 'semantic-ui-react';
import '../style/customInput.css';

function CustomInput(props) {
    const {
        label,
        placeholder,
        type,
        value,
        onChangeFunction,
        className,
        viewOnly,
    } = props;

    var addedClass = '';
    addedClass += className ? className : '';
    addedClass += viewOnly ? ' view-only-input' : '';

    return (
        <div className={'input-container ' + addedClass}>
            <p className="label-input">{label}</p>
            <Input
                placeholder={placeholder}
                type={type}
                className="input-custom"
                value={value}
                onChange={onChangeFunction}
                viewonly={viewOnly ? true : false}
            />
        </div>
    );
}
export default CustomInput;
