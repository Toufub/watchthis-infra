import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Modal } from 'semantic-ui-react';
import { closeModal } from '../core/actions/modal.actions';
import {
    selectorFilmInModal,
    selectorShowModal,
} from '../core/selector/modal.selector';
import FilmCard from './FilmCard';
import Config from '../config';
import { XMarkIcon } from '@heroicons/react/24/solid';

import '../style/customModal.css';

function CustomModal() {
    const dispatch = useDispatch();

    const open = useSelector(selectorShowModal);
    const film = useSelector(selectorFilmInModal);

    return (
        <Modal
            onClose={() => dispatch(closeModal())}
            open={open}
            id="custom-modal"
            dimmer="blurring"
        >
            <Modal.Content
                className="modal-container"
                id="custom-modal-content"
            >
                <XMarkIcon
                    className="h-6 h-6 modal-close"
                    onClick={() => dispatch(closeModal())}
                />
                <FilmCard
                    displayMode={Config.FILM_LIST_MODE.DETAIL}
                    film={film}
                    className="m-0"
                />
            </Modal.Content>
        </Modal>
    );
}

export default CustomModal;
