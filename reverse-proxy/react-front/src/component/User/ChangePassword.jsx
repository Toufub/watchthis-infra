import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { Button, Grid } from 'semantic-ui-react';
import {
    selectorConnectedUser,
    selectorUserToken,
} from '../../core/selector/user.selector';
import UserService from '../../services/userService';
import CustomInput from '../CustomInput';

function ChangePassword() {
    const user = useSelector(selectorConnectedUser);
    const token = useSelector(selectorUserToken);

    const [password, setPwd] = useState('');
    const [newPassword, setNewPassword] = useState('');
    const [newPasswordConfirm, setNewPasswordConfirm] = useState('');
    const [error, setError] = useState('');
    const [success, setSuccess] = useState('');

    const onModifierClick = () => {
        setError('');
        setSuccess('');
        UserService.updatePassword(
            token,
            password,
            newPassword,
            newPasswordConfirm,
            user.utilisateurId,
            setError,
            inSuccess
        );
    };

    const inSuccess = () => {
        setSuccess('Votre mots de passe à bien été changé');
        setNewPassword('');
        setNewPasswordConfirm('');
        setPwd('');
    };

    function displayError() {
        if (error.length !== 0) {
            return (
                <Grid.Row columns={1}>
                    <Grid.Column>
                        <p className="error">{error}</p>
                    </Grid.Column>
                </Grid.Row>
            );
        }
    }

    function displaySuccess() {
        if (success.length !== 0) {
            return (
                <Grid.Row columns={1}>
                    <Grid.Column>
                        <p className="success">{success}</p>
                    </Grid.Column>
                </Grid.Row>
            );
        }
    }

    return (
        <div
            className="ui middle aligned center aligned grid"
            style={{ height: '100%' }}
        >
            <div
                className="column"
                style={{ maxWidth: '85%', marginTop: '1em' }}
            >
                <Grid className="edit-user-form">
                    {displayError()}
                    {displaySuccess()}
                    <Grid.Row columns={1}>
                        <Grid.Column>
                            <CustomInput
                                label="Ancien mot de passe"
                                placeholder="**********"
                                type="password"
                                onChangeFunction={(e) => setPwd(e.target.value)}
                                value={password}
                            />
                        </Grid.Column>
                    </Grid.Row>

                    <Grid.Row columns={1}>
                        <Grid.Column>
                            <CustomInput
                                label="Nouveau mot de passe"
                                placeholder="**********"
                                type="password"
                                onChangeFunction={(e) =>
                                    setNewPassword(e.target.value)
                                }
                                value={newPassword}
                            />
                        </Grid.Column>
                    </Grid.Row>

                    <Grid.Row columns={1}>
                        <Grid.Column>
                            <CustomInput
                                label="Confirmation nouveau mot de passe"
                                placeholder="**********"
                                type="password"
                                onChangeFunction={(e) =>
                                    setNewPasswordConfirm(e.target.value)
                                }
                                value={newPasswordConfirm}
                            />
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row columns={1}>
                        <Grid.Column>
                            <Button
                                className="modify-button btn"
                                onClick={() => onModifierClick()}
                            >
                                Modifier
                            </Button>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        </div>
    );
}

export default ChangePassword;
