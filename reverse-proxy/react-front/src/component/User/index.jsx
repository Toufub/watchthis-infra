import React from 'react';
import {
    Button,
    Card,
    Grid,
    Header,
    Image,
    Menu,
    Segment,
} from 'semantic-ui-react';

import '../../style/user.css';
import Config from '../../config';
import UserEditForm from './UserEditForm';
import ChangePassword from './ChangePassword';
import UserStatistique from './UserStatistique';

import { XMarkIcon } from '@heroicons/react/24/solid';

function User(props) {
    const { displayMode, user } = props;

    function getImageUser() {
        return user.imageProfil
            ? user.imageProfil
            : window.location.origin + '/images/defaultAvatar.jpg';
    }

    function getDate() {
        let date = new Date(user.dateCreation);
        return date.toLocaleDateString();
    }

    const displayInSideBar = () => {
        return (
            <Menu.Item
                onClick={() => {}}
                className="menu-user-container nav-item-container"
            >
                <img
                    src={getImageUser()}
                    alt="User avatar"
                    className="rounded-user-img"
                />
                <p>{user.nom + ' ' + user.prenom}</p>
            </Menu.Item>
        );
    };

    const displayInHeader = () => {
        return (
            <Menu.Item onClick={() => {}} className="menu-user-container">
                <img
                    src={getImageUser()}
                    alt="User avatar"
                    className="rounded-user-img"
                />
                <p>{user.nom + ' ' + user.prenom}</p>
            </Menu.Item>
        );
    };

    const displayInChat = () => {
        return (
            <>
                <Image
                    floated="left"
                    size="mini"
                    src={getImageUser()}
                    circular
                    className="user-img"
                />
                <Card.Header>{user.nom + ' ' + user.prenom}</Card.Header>
            </>
        );
    };

    const displayInEditForm = () => {
        return <UserEditForm />;
    };

    const displayInChangePassword = () => {
        return <ChangePassword />;
    };

    const displayInStat = () => {
        return <UserStatistique />;
    };

    const displayInFreinds = () => {
        return (
            <>
                <img
                    src={getImageUser()}
                    alt="User avatar"
                    className="rounded-user-img imgProfileInChat"
                />
                <Card.Description>
                    <Header as="h5">{user.nom + ' ' + user.prenom}</Header>
                    <Card.Meta>{user.mail}</Card.Meta>
                    <Card.Meta>{getDate()}</Card.Meta>
                </Card.Description>
            </>
        );
    };

    if (displayMode === Config.USER_DIPSPLAY_MODE.SIDEBAR) {
        return displayInSideBar();
    } else if (displayMode === Config.USER_DIPSPLAY_MODE.HEADER) {
        return displayInHeader();
    } else if (displayMode === Config.USER_DIPSPLAY_MODE.CHAT) {
        return displayInChat();
    } else if (displayMode === Config.USER_DIPSPLAY_MODE.EDIT_FORM) {
        return displayInEditForm();
    } else if (displayMode === Config.USER_DIPSPLAY_MODE.CHANGE_PASSWORD) {
        return displayInChangePassword();
    } else if (displayMode === Config.USER_DIPSPLAY_MODE.STATS) {
        return displayInStat();
    } else if (displayMode === Config.USER_DIPSPLAY_MODE.FREINDS) {
        return displayInFreinds();
    }
}

export default User;
