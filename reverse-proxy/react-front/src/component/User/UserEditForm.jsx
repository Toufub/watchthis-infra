import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Button, Checkbox, Grid, Header, Image } from 'semantic-ui-react';
import {
    selectorConnectedUser,
    selectorUserToken,
} from '../../core/selector/user.selector';
import CustomInput from '../CustomInput';

import '../../style/user.css';
import UserService from '../../services/userService';
import { openConnectModal } from '../../core/actions/modal.actions';
import ConnectModal from '../ConnectModal';
import { ArrowPathIcon } from '@heroicons/react/24/outline';

function UserEditForm() {
    const user = useSelector(selectorConnectedUser);
    const token = useSelector(selectorUserToken);

    const dispatch = useDispatch();

    const [error, setError] = useState('');
    const [success, setSuccess] = useState('');

    const [userId, setUserId] = useState('');
    const [betaSeriesId, setBetaSeriesId] = useState('');
    const [nom, setNom] = useState('');
    const [prenom, setPrenom] = useState('');
    const [email, setEmail] = useState('');
    const [dateCreation, setDateCreation] = useState('');
    const [imageProfil, setImageProfil] = useState('');
    const [notificationActive, setNotificationActive] = useState(false);

    useEffect(() => {
        setUserId(user.utilisateurId);
        setBetaSeriesId(user.betaSeriesId);
        setNom(user.nom);
        setPrenom(user.prenom);
        setEmail(user.mail);
        setDateCreation(user.dateCreation);
        setImageProfil(user.imageProfil);
        setNotificationActive(user.notificationActive);
    }, [user]);

    const onEditSucess = (cxt) => {
        setSuccess('Votre compte a bien été modifié !');
    };

    const onModifierClick = () => {
        setError('');
        UserService.updateUser(
            token,
            dispatch,
            userId,
            nom,
            prenom,
            email,
            imageProfil,
            notificationActive,
            betaSeriesId,
            setError,
            onEditSucess
        );
    };

    function displayError() {
        if (error.length !== 0) {
            return (
                <Grid.Row columns={1}>
                    <Grid.Column>
                        <p className="error">{error}</p>
                    </Grid.Column>
                </Grid.Row>
            );
        }
    }

    function displaySuccess() {
        if (success.length !== 0) {
            return (
                <Grid.Row columns={1}>
                    <Grid.Column>
                        <p className="success">{success}</p>
                    </Grid.Column>
                </Grid.Row>
            );
        }
    }

    const onConnect = () => {
        dispatch(openConnectModal());
    };

    function isBetaSerieId() {
        if (betaSeriesId) {
            return (
                <CustomInput
                    label="BetaSeries ID"
                    type="text"
                    value={betaSeriesId}
                    className="p-0"
                    viewOnly
                />
            );
        } else {
            return (
                <div className={'input-container p-0'}>
                    <p className="label-input">BetaSeries ID</p>
                    <Button
                        circular
                        onClick={() => onConnect()}
                        className="btn primary-btn  w-100"
                    >
                        Connexion
                    </Button>
                </div>
            );
        }
    }

    return (
        <div
            className="ui middle aligned center aligned grid"
            style={{ height: '100%' }}
        >
            <div
                className="column"
                style={{ maxWidth: '85%', marginTop: '1em' }}
            >
                <Grid className="edit-user-form">
                    {displayError()}
                    {displaySuccess()}
                    <Grid.Row columns={2}>
                        <Grid.Column>
                            <CustomInput
                                label="Utilisateur ID"
                                type="text"
                                value={userId}
                                className="p-0"
                                viewOnly
                            />
                        </Grid.Column>
                        <Grid.Column>{isBetaSerieId()}</Grid.Column>
                    </Grid.Row>
                    <Grid.Row columns={2}>
                        <Grid.Column>
                            <CustomInput
                                label="Nom"
                                placeholder="Doe"
                                type="text"
                                onChangeFunction={(e) => setNom(e.target.value)}
                                value={nom}
                                className="p-0"
                            />
                        </Grid.Column>
                        <Grid.Column>
                            <CustomInput
                                label="Prenom"
                                placeholder="John"
                                type="text"
                                onChangeFunction={(e) =>
                                    setPrenom(e.target.value)
                                }
                                value={prenom}
                                className="p-0"
                            />
                        </Grid.Column>
                    </Grid.Row>

                    <Grid.Row columns={1}>
                        <Grid.Column>
                            <CustomInput
                                label="Email"
                                placeholder="exemple@mail.com"
                                type="text"
                                onChangeFunction={(e) =>
                                    setEmail(e.target.value)
                                }
                                value={email}
                                className="p-0"
                            />
                        </Grid.Column>
                    </Grid.Row>

                    <Grid.Row columns={1}>
                        <Grid.Column>
                            <CustomInput
                                label="Photos de profil url"
                                placeholder="https://masuperphoto.com/monimage.png"
                                type="text"
                                onChangeFunction={(e) =>
                                    setImageProfil(e.target.value)
                                }
                                value={imageProfil}
                                className="p-0"
                            />
                        </Grid.Column>
                    </Grid.Row>

                    <Grid.Row columns={2}>
                        <Grid.Column>
                            <p className="fs-16">
                                Date de création : {dateCreation}
                            </p>
                        </Grid.Column>
                        <Grid.Column>
                            <Checkbox
                                label="Activer les notifications"
                                toggle
                                onChange={() =>
                                    setNotificationActive(!notificationActive)
                                }
                                checked={notificationActive}
                                className="fs-16"
                            />
                        </Grid.Column>
                    </Grid.Row>

                    <Grid.Row columns={1}>
                        <Grid.Column>
                            <Button
                                className="modify-button btn"
                                onClick={() => onModifierClick()}
                            >
                                Modifier
                            </Button>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
                <ConnectModal />
            </div>
        </div>
    );
}

export default UserEditForm;
