import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Button, Grid } from 'semantic-ui-react';
import {
    selectorFilmAVoir,
    selectorFilmDejaVue,
} from '../../core/selector/film.selector';

import { ArrowPathIcon, TrashIcon } from '@heroicons/react/24/solid';

import '../../style/user.css';
import UserService from '../../services/userService';
import {
    selectorConnectedUser,
    selectorUserToken,
} from '../../core/selector/user.selector';
import FilmService from '../../services/filmService';
import Config from '../../config';
import ConfirmModal from '../ConfirmModal';

function UserStatistique() {
    const dispatch = useDispatch();

    const [success, setSuccess] = useState('');
    const [error, setError] = useState('');
    const [openConfirmDeleteFilm, setOpenConfirmDeleteFilm] = useState(false);

    const filmAVoir = useSelector(selectorFilmAVoir);
    const filmDejaVue = useSelector(selectorFilmDejaVue);

    const user = useSelector(selectorConnectedUser);
    const token = useSelector(selectorUserToken);

    const onSuccess = (msg) => {
        setSuccess(msg);
        FilmService.getFilmByCategorisationAndUser(
            token,
            Config.CATEGORISATIONS.DEJA_VUE.VALUE,
            setError,
            dispatch
        );
        FilmService.getFilmByCategorisationAndUser(
            token,
            Config.CATEGORISATIONS.VOIR.VALUE,
            setError,
            dispatch
        );
    };

    const showDeleteFilmConfirm = () => {
        setOpenConfirmDeleteFilm(true);
    };

    const onDeleteFilm = () => {
        FilmService.deleteAllCAtegorisation(
            token,
            user.utilisateurId,
            dispatch
        );
    };

    const onSync = () => {
        UserService.syncToBetaSeries(token, onSuccess);
    };

    function displaySuccess() {
        if (success.length !== 0) {
            return (
                <Grid.Row columns={1}>
                    <Grid.Column>
                        <p className="success">{success}</p>
                    </Grid.Column>
                </Grid.Row>
            );
        }
    }

    function displayError() {
        if (error.length !== 0) {
            return (
                <Grid.Row columns={1}>
                    <Grid.Column>
                        <p className="error">{error}</p>
                    </Grid.Column>
                </Grid.Row>
            );
        }
    }

    const displaySyncRow = () => {
        if (user.betaSeriesId) {
            return (
                <Grid.Row columns={2}>
                    <Grid.Column>
                        <Button
                            className="btn secondary-btn sync h-100"
                            onClick={() => onSync()}
                            fluid
                        >
                            <ArrowPathIcon className="h-6 h-6 icon white-icon" />

                            <p className="fs-16">
                                Synchronysée avec beta series
                            </p>
                        </Button>
                    </Grid.Column>
                    <Grid.Column>
                        <Button
                            className="btn primary-btn sync h-100"
                            onClick={() => showDeleteFilmConfirm()}
                            fluid
                        >
                            <TrashIcon className="h-6 h-6 icon white-icon" />

                            <p className="fs-16">Supprimer mes films</p>
                        </Button>
                    </Grid.Column>
                </Grid.Row>
            );
        } else {
            <Grid.Row columns={1}>
                <Grid.Column>
                    <Button
                        className="btn primary-btn sync h-100"
                        onClick={() => showDeleteFilmConfirm()}
                        fluid
                    >
                        <TrashIcon className="h-6 h-6 icon white-icon" />

                        <p className="fs-16">Supprimer mes films</p>
                    </Button>
                </Grid.Column>
            </Grid.Row>;
        }
    };

    return (
        <>
            <div
                className="ui middle aligned center aligned grid"
                style={{ height: '100%' }}
            >
                <div
                    className="column"
                    style={{ maxWidth: '85%', marginTop: '1em' }}
                >
                    <Grid className="view-user-form">
                        {displaySuccess()}
                        {displayError()}
                        <Grid.Row columns={2}>
                            <Grid.Column>
                                <p className="fs-16">
                                    Vos film déja vue :{' '}
                                    {filmDejaVue ? filmDejaVue.length : '0'}
                                </p>
                            </Grid.Column>
                            <Grid.Column>
                                <p className="fs-16">
                                    Vos film a voir :{' '}
                                    {filmAVoir ? filmAVoir.length : '0'}
                                </p>
                            </Grid.Column>
                        </Grid.Row>
                        {displaySyncRow()}
                    </Grid>
                </div>
            </div>
            <ConfirmModal
                title="Supprimer vos films"
                content="Vos etes sur le point de supprimer vos films vue, à voir et non intéresser"
                callbackSuccess={onDeleteFilm}
                setOpen={setOpenConfirmDeleteFilm}
                open={openConfirmDeleteFilm}
                confirmLabel="Supprimer"
            />
        </>
    );
}

export default UserStatistique;
