import React from 'react';
import { Button, Modal } from 'semantic-ui-react';

function ConfirmModal(props) {
    const { title, content, setOpen, callbackSuccess, open, confirmLabel } =
        props;

    //const open = useSelector(selectorShowConfirmModal);

    return (
        <Modal size="mini" open={open} onClose={() => setOpen(false)}>
            <Modal.Header>{title}</Modal.Header>
            <Modal.Content>
                <p>{content}</p>
            </Modal.Content>
            <Modal.Actions>
                <Button negative onClick={() => setOpen(false)}>
                    Annuler
                </Button>
                <Button
                    positive
                    onClick={() => {
                        callbackSuccess();
                        setOpen(false);
                    }}
                >
                    {confirmLabel}
                </Button>
            </Modal.Actions>
        </Modal>
    );
}

export default ConfirmModal;
