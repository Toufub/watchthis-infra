import React from 'react';
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { Button } from 'semantic-ui-react';
import { selectorConnectedUser } from '../core/selector/user.selector';

import '../style/home.css';

function Home() {
    const user = useSelector(selectorConnectedUser);

    const navigate = useNavigate();

    const onConnectionClick = () => {
        navigate('/login');
    };

    const displayConnectionButton = () => {
        if (!user) {
            return (
                <Button
                    circular
                    id="connection-button"
                    onClick={() => onConnectionClick()}
                    className="btn"
                >
                    Se connecter
                </Button>
            );
        }
    };

    return (
        <>
            <img
                src={window.location.origin + '/images/home-background.jpg'}
                alt="Home background"
                className="back-ground"
            />
            <div
                className="ui middle aligned center aligned grid"
                style={{ height: '100%' }}
            >
                <div
                    className="column"
                    style={{ maxWidth: '700px', marginTop: '10em' }}
                >
                    <div className="home-msg">
                        <h1>Bienvenue sur Watch th!s</h1>
                        <p className="fs-16">Connectez vous afin de :</p>
                        <p className="fs-16">
                            Partager vos films avec vos amis, trouver des films
                            spécialement recommandés pour vous,
                        </p>
                        <p className="fs-16">Et bien plus encore !</p>
                        {displayConnectionButton()}
                    </div>
                </div>
            </div>
        </>
    );
}

export default Home;
