import Config from '../config';
import { socket } from '.';
import { swipeSetNextFilm } from '../core/actions/swipe.actions';
import FilmService from '../services/filmService';
class RecommandationWSService {
    static _instance;

    constructor(dispacth, token) {
        if (socket !== null) {
            this.socket = socket;

            //When a recomendation arrives
            socket.on(Config.SOCKET_EVENT.FILMS_RECOMMANDES, (films) => {
                FilmService.getFilmByIdsForSwipe(token, films, dispacth);
            });

            //join confirmation
            socket.on(Config.SOCKET_EVENT.CONFIRMATION_MESSAGE, (msg) => {
                if (msg === 'ok') console.log('connection a recommendation ok');
            });

            //join forbbiden
            socket.on(Config.SOCKET_EVENT.CONFIRMATION_MESSAGE, (msg) => {
                if (msg === 'already-use')
                    console.error(
                        'erreur lors de la connexion a recommendation'
                    );
            });

            this._instance = this;
        } else {
            throw new Error(
                'Impossible de creer un Recomendation Service sans socket'
            );
        }
        return RecommandationWSService._instance;
    }

    static getInstance(dispacth, token) {
        if (!RecommandationWSService._instance) {
            this._instance = new RecommandationWSService(dispacth, token);
            return this._instance;
        } else {
            return this._instance;
        }
    }

    sendUserLogin(user) {
        this.socket.emit(Config.SOCKET_EVENT.USER_LOGIN, user);
    }
}
export default RecommandationWSService;
