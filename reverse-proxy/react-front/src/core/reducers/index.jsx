import { FaceSmileIcon } from '@heroicons/react/24/outline';
import { filmAction } from '../actions/films.actions';
import { menuAction } from '../actions/menu.actions';
import { modalAction } from '../actions/modal.actions';
import { publicationAction } from '../actions/publications.action';
import { swipeAction } from '../actions/swipe.actions';
import { userAction } from '../actions/user.actions';

const initStateValue = {
    menuActiveItemKey: 'dejaVue',
    showModal: false,
    filmInModal: {},
    userConnected: null,
    showRatingModal: false,
    filmInRatingModal: false,
    userToken: null,
    showConfirmModal: false,
    filmAVoir: [],
    filmDejaVue: [],
    showConnectModal: false,
    filmsInCommunaute: [],
    userInCommunaute: [],
    publicationInCommunaute: [],
    displayModeInRatingModal: null,
    userFreinds: [],
    swipeIndex: 0,
    swipeFilm: [],
    nextSwipeFilm: [],
};

export const rootReducer = (state = initStateValue, action) => {
    if (action.type === menuAction.SET_ACTIVE_ITEM) {
        // console.log('Store : { payload : ' + action.payload.cards + ', type : ' + action.type + '}');
        return {
            ...state,
            menuActiveItemKey: action.payload.itemActif,
        };
    }

    if (action.type === modalAction.SET_FILM_IN_MODAL) {
        return {
            ...state,
            filmInModal: action.payload.film,
            showModal: true,
        };
    }

    if (action.type === modalAction.CLOSE_MODAL) {
        return {
            ...state,
            showModal: false,
        };
    }

    if (action.type === userAction.CONNECT_USER) {
        return {
            ...state,
            userConnected: action.payload.user,
        };
    }

    if (action.type === userAction.DISCONNECT_USER) {
        return {
            ...state,
            userConnected: null,
            userToken: null,
        };
    }

    if (action.type === modalAction.CLOSE_RATING_MODAL) {
        return {
            ...state,
            showRatingModal: false,
            displayModeInRatingModal: null,
        };
    }

    if (action.type === modalAction.SET_FILM_IN_RATING_MODAL) {
        return {
            ...state,
            filmInRatingModal: action.payload.film,
            displayModeInRatingModal: action.payload.mode,
            showRatingModal: true,
        };
    }

    if (action.type === userAction.SET_USER_TOKEN) {
        return {
            ...state,
            userToken: action.payload.token,
        };
    }

    if (action.type === modalAction.CLOSE_CONFIRM_MODAL) {
        return {
            ...state,
            showConfirmModal: false,
        };
    }

    if (action.type === modalAction.OPEN_CONFIRM_MODAL) {
        return {
            ...state,
            showConfirmModal: true,
        };
    }

    if (action.type === modalAction.CLOSE_CONNECT_MODAL) {
        return {
            ...state,
            showConnectModal: false,
        };
    }

    if (action.type === modalAction.OPEN_CONNECT_MODAL) {
        return {
            ...state,
            showConnectModal: true,
        };
    }

    if (action.type === filmAction.SET_FILM_A_VOIR) {
        return {
            ...state,
            filmAVoir: action.payload.films,
        };
    }

    if (action.type === filmAction.SET_FILM_DEJA_VUE) {
        return {
            ...state,
            filmDejaVue: action.payload.films,
        };
    }

    if (action.type === filmAction.SET_FILM_FOR_PUBLICATION_PAGE) {
        return {
            ...state,
            filmsInCommunaute: action.payload.films,
        };
    }

    if (action.type === userAction.SET_USER_FOR_PUBLICATION_PAGE) {
        return {
            ...state,
            userInCommunaute: action.payload.users,
        };
    }

    if (action.type === publicationAction.SET_PUBLICATIONS) {
        return {
            ...state,
            publicationInCommunaute: action.payload.publications,
        };
    }

    if (action.type === userAction.SET_USER_FREINDS) {
        return {
            ...state,
            userFreinds: action.payload.freinds,
        };
    }

    if (action.type === swipeAction.NEXT_INDEX) {
        const tempIndex = state.swipeIndex + 1;
        return {
            ...state,
            swipeIndex: tempIndex,
        };
    }

    if (action.type === swipeAction.RESET_INDEX) {
        return {
            ...state,
            swipeIndex: 0,
        };
    }

    if (action.type === swipeAction.GET_NEXT_FILM) {
        return {
            ...state,
            swipeFilm: state.nextSwipeFilm,
        };
    }

    if (action.type === swipeAction.SET_NEXT_FILM) {
        return {
            ...state,
            nextSwipeFilm: action.payload.films,
        };
    }

    return state;
};
