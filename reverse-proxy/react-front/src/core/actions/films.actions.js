export const filmAction = {
    GET_FILM: '@@film/GET_FILM',
    SET_FILM_DEJA_VUE: '@@film/SET_FILM_DEJA_VUE',
    SET_FILM_A_VOIR: '@@film/SET_FILM_A_VOIR',
    SET_FILM_FOR_PUBLICATION_PAGE: '@@user/SET_FILM_FOR_PUBLICATION_PAGE',
};

export const getFilm = (films) => {
    return {
        type: filmAction.GET_FILM,
        payload: {
            films,
        },
    };
};

export const setFilmDejaVue = (films) => {
    return {
        type: filmAction.SET_FILM_DEJA_VUE,
        payload: {
            films,
        },
    };
};

export const setFilmAVoir = (films) => {
    return {
        type: filmAction.SET_FILM_A_VOIR,
        payload: {
            films,
        },
    };
};

export const setFilmsForPublicationsPage = (films) => {
    return {
        type: filmAction.SET_FILM_FOR_PUBLICATION_PAGE,
        payload: { films },
    };
};
