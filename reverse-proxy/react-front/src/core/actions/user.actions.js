export const userAction = {
    CONNECT_USER: '@@user/CONNECT_USER',
    DISCONNECT_USER: '@@user/CONNECT_USER',
    SET_USER_TOKEN: '@@user/SET_USER_TOKEN',
    SET_USER_FOR_PUBLICATION_PAGE: '@@user/SET_USER_FOR_PUBLICATION_PAGE',
    SET_USER_FREINDS: '@@user/SET_USER_FREINDS',
};

export const connectUser = (user) => {
    return {
        type: userAction.CONNECT_USER,
        payload: {
            user,
        },
    };
};

export const disconnectUser = () => {
    return {
        type: userAction.DISCONNECT_USER,
        payload: {},
    };
};

export const setUserToken = (token) => {
    return {
        type: userAction.SET_USER_TOKEN,
        payload: { token },
    };
};

export const setUserForPublicationsPage = (users) => {
    return {
        type: userAction.SET_USER_FOR_PUBLICATION_PAGE,
        payload: { users },
    };
};

export const setUserFreinds = (freinds) => {
    return {
        type: userAction.SET_USER_FREINDS,
        payload: { freinds },
    };
};
