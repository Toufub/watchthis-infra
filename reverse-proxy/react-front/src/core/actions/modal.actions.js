export const modalAction = {
    SET_FILM_IN_MODAL: '@@modal/SET_FILM_IN_MODAL',
    CLOSE_MODAL: '@@modal/CLOSE_MODAL',
    SET_FILM_IN_RATING_MODAL: '@@modal/SET_FILM_IN_RATING_MODAL',
    CLOSE_RATING_MODAL: '@@modal/CLOSE_RATING_MODAL',
    CLOSE_CONFIRM_MODAL: '@@modal/CLOSE_CONFIRM_MODAL',
    OPEN_CONFIRM_MODAL: '@@modal/OPEN_CONFIRM_MODAL',
    CLOSE_CONNECT_MODAL: '@@modal/CLOSE_CONNECT_MODAL',
    OPEN_CONNECT_MODAL: '@@modal/OPEN_CONNECT_MODAL',
};

export const setFilmInModal = (film) => {
    return {
        type: modalAction.SET_FILM_IN_MODAL,
        payload: {
            film,
        },
    };
};

export const closeModal = () => {
    return {
        type: modalAction.CLOSE_MODAL,
        payload: {},
    };
};

export const setFilmInRatingModal = (film, mode) => {
    return {
        type: modalAction.SET_FILM_IN_RATING_MODAL,
        payload: {
            film,
            mode,
        },
    };
};

export const closeRatingModal = () => {
    return {
        type: modalAction.CLOSE_RATING_MODAL,
        payload: {},
    };
};

export const openConfirmModal = () => {
    return {
        type: modalAction.OPEN_CONFIRM_MODAL,
        payload: {},
    };
};

export const closeConfirmModal = () => {
    return {
        type: modalAction.CLOSE_CONFIRM_MODAL,
        payload: {},
    };
};

export const openConnectModal = () => {
    return {
        type: modalAction.OPEN_CONNECT_MODAL,
        payload: {},
    };
};

export const closeConnectModal = () => {
    return {
        type: modalAction.CLOSE_CONNECT_MODAL,
        payload: {},
    };
};
