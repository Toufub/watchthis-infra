export const menuAction = {
    SET_ACTIVE_ITEM: '@@menu/SET_ACTIVE_ITEM',
};

export const setActiveItem = (itemActif) => {
    return {
        type: menuAction.SET_ACTIVE_ITEM,
        payload: {
            itemActif,
        },
    };
};
