export const publicationAction = {
    SET_PUBLICATIONS: '@@publication/SET_PUBLICATIONS',
};

export const setPublication = (publications) => {
    return {
        type: publicationAction.SET_PUBLICATIONS,
        payload: { publications },
    };
};
