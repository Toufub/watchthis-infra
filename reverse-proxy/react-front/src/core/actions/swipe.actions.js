export const swipeAction = {
    RESET_INDEX: '@@swipe/RESET_INDEX',
    NEXT_INDEX: '@@swipe/NEXT_INDEX',
    GET_NEXT_FILM: '@@swipe/GET_NEXT_FILM',
    SET_NEXT_FILM: '@@swipe/SET_NEXT_FILM',
};

export const swipeNextIndex = () => {
    return {
        type: swipeAction.NEXT_INDEX,
        payload: {},
    };
};

export const swipeResetIndex = () => {
    return {
        type: swipeAction.RESET_INDEX,
        payload: {},
    };
};

export const swipeGetNextFilm = () => {
    return {
        type: swipeAction.GET_NEXT_FILM,
        payload: {},
    };
};

export const swipeSetNextFilm = (films) => {
    return {
        type: swipeAction.SET_NEXT_FILM,
        payload: { films },
    };
};
