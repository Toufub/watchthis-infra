export const selectorShowModal = (state) => state.showModal;

export const selectorFilmInModal = (state) => state.filmInModal;

export const selectorShowRatingModal = (state) => state.showRatingModal;

export const selectorFilmRatingModal = (state) => state.filmInRatingModal;

export const selectorDisplayModeInRatingModal = (state) =>
    state.displayModeInRatingModal;

export const selectorShowConfirmModal = (state) => state.showConfirmModal;

export const selectorShowConnectModal = (state) => state.showConnectModal;
