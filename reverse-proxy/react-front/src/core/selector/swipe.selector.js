export const selectorSwipeIndex = (state) => state.swipeIndex;

export const selectorSwipeFilm = (state) => state.swipeFilm;

export const selectorNextSwipeFilm = (state) => state.nextSwipeFilm;
