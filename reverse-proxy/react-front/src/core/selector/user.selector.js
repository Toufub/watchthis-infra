export const selectorConnectedUser = (state) => state.userConnected;

export const selectorUserToken = (state) => state.userToken;

export const selectorUserInCommunaute = (state) => state.userInCommunaute;

export const selectorUserFreinds = (state) => state.userFreinds;
