export const selectorFilmAVoir = (state) => state.filmAVoir;

export const selectorFilmDejaVue = (state) => state.filmDejaVue;

export const selectorFilmInCommunaute = (state) => state.filmsInCommunaute;
