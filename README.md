# Watch this - Infra + Front React

Le code du react se trouve dans /reverse-proxy/react-front/

# DEPOTS GIT

* Infra + React : https://gitlab.com/Toufub/watchthis-infra
* Notifications : https://gitlab.com/Toufub/watchthis-backend-notifications
* Recommandations : https://gitlab.com/Toufub/watchthis-backend-recommandations
* Publications : https://gitlab.com/Toufub/watchthis-backend-publications
* Authentification : https://gitlab.com/Toufub/watchthis-backend-auth
* Films : https://gitlab.com/Toufub/watchthis-backend-films
* Utilisateurs : https://gitlab.com/Toufub/watchthis-backend-utilisateurs

Pour executer l'application : Git clone de l'infra et exécuter RUN.BAT (Windows)

Sinon sous linux dans le dossier Watchthis-infra :
* cd ./reverse-proxy/react-front/
* npm run build
* cd ../../
* docker compose build --pull
* docker compose up